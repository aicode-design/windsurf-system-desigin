# RBAC+ABAC混合权限管理系统详细设计文档

## 目录

1. [系统详细设计](#1-系统详细设计)
   - [1.1 核心类设计](#11-核心类设计)
   - [1.2 数据库表详细设计](#12-数据库表详细设计)
   - [1.3 API详细设计](#13-api详细设计)
   - [1.4 缓存设计](#14-缓存设计)

2. [时序图](#2-时序图)
   - [2.1 权限验证流程](#21-权限验证流程)
   - [2.2 用户认证流程](#22-用户认证流程)
   - [2.3 角色分配流程](#23-角色分配流程)
   - [2.4 权限检查流程](#24-权限检查流程)

3. [部署架构](#3-部署架构)
   - [3.1 容器化部署](#31-容器化部署)
   - [3.2 高可用设计](#32-高可用设计)
   - [3.3 负载均衡](#33-负载均衡)
   - [3.4 服务发现](#34-服务发现)

4. [安全设计](#4-安全设计)
   - [4.1 密码安全](#41-密码安全)
   - [4.2 JWT配置](#42-jwt配置)
   - [4.3 加密算法](#43-加密算法)
   - [4.4 安全策略](#44-安全策略)

5. [性能优化](#5-性能优化)
   - [5.1 数据库索引优化](#51-数据库索引优化)
   - [5.2 缓存策略](#52-缓存策略)
   - [5.3 并发处理](#53-并发处理)
   - [5.4 性能监控](#54-性能监控)

6. [测试策略](#6-测试策略)
   - [6.1 单元测试](#61-单元测试)
   - [6.2 集成测试](#62-集成测试)
   - [6.3 性能测试](#63-性能测试)
   - [6.4 安全测试](#64-安全测试)

7. [错误处理](#7-错误处理)
   - [7.1 全局异常处理](#71-全局异常处理)
   - [7.2 错误码设计](#72-错误码设计)
   - [7.3 日志记录](#73-日志记录)
   - [7.4 告警机制](#74-告警机制)

8. [系统配置](#8-系统配置)
   - [8.1 应用配置](#81-应用配置)
   - [8.2 环境配置](#82-环境配置)
   - [8.3 日志配置](#83-日志配置)
   - [8.4 监控配置](#84-监控配置)

9. [部署指南](#9-部署指南)
   - [9.1 环境要求](#91-环境要求)
   - [9.2 部署步骤](#92-部署步骤)
   - [9.3 配置说明](#93-配置说明)
   - [9.4 验证部署](#94-验证部署)

10. [维护指南](#10-维护指南)
    - [10.1 日常维护任务](#101-日常维护任务)
    - [10.2 监控检查清单](#102-监控检查清单)
    - [10.3 故障恢复流程](#103-故障恢复流程)
    - [10.4 升级流程](#104-升级流程)

11. [TypeScript实现](#11-typescript实现)
    - [11.1 核心模型](#111-核心模型)
    - [11.2 服务实现](#112-服务实现)
    - [11.3 工具类](#113-工具类)
    - [11.4 中间件](#114-中间件)

12. [前端实现](#12-前端实现)
    - [12.1 组件设计](#121-组件设计)
    - [12.2 状态管理](#122-状态管理)
    - [12.3 路由设计](#123-路由设计)
    - [12.4 权限控制](#124-权限控制)

13. [API文档](#13-api文档)
    - [13.1 认证接口](#131-认证接口)
    - [13.2 用户接口](#132-用户接口)
    - [13.3 角色接口](#133-角色接口)
    - [13.4 权限接口](#134-权限接口)

14. [数据迁移](#14-数据迁移)
    - [14.1 迁移策略](#141-迁移策略)
    - [14.2 数据备份](#142-数据备份)
    - [14.3 回滚计划](#143-回滚计划)
    - [14.4 验证流程](#144-验证流程)

15. [监控告警](#15-监控告警)
    - [15.1 监控指标](#151-监控指标)
    - [15.2 告警规则](#152-告警规则)
    - [15.3 告警通道](#153-告警通道)
    - [15.4 告警级别](#154-告警级别)

16. [多语言实现](#16-多语言实现)
    - [16.1 Python实现](#161-python实现)
    - [16.2 Golang实现](#162-golang实现)
    - [16.3 Kotlin实现](#163-kotlin实现)
    - [16.4 Scala实现](#164-scala实现)
    - [16.5 Groovy实现](#165-groovy实现)

## 10. 维护指南

### 10.1 日常维护任务

```typescript
interface MaintenanceTasks {
    // 数据库维护
    readonly databaseTasks = {
        daily: [
            '检查数据库连接池状态',
            '监控慢查询日志',
            '检查数据库空间使用'
        ],
        weekly: [
            '清理过期会话数据',
            '优化数据库索引',
            '备份数据库'
        ],
        monthly: [
            '清理审计日志',
            '检查数据库性能',
            '更新数据库统计信息'
        ]
    };

    // 缓存维护
    readonly cacheTasks = {
        daily: [
            '监控缓存命中率',
            '检查缓存内存使用',
            '清理过期缓存'
        ],
        weekly: [
            '分析缓存访问模式',
            '优化缓存策略',
            '备份Redis数据'
        ]
    };

    // 系统维护
    readonly systemTasks = {
        daily: [
            '检查系统日志',
            '监控系统资源',
            '检查服务状态'
        ],
        weekly: [
            '更新安全补丁',
            '检查证书有效期',
            '清理临时文件'
        ],
        monthly: [
            '系统性能评估',
            '更新系统文档',
            '安全漏洞扫描'
        ]
    };
}
```

### 10.2 监控检查清单

```typescript
interface MonitoringChecklist {
    // 系统指标
    readonly systemMetrics = {
        cpu: {
            usage: '< 80%',
            load: '< 系统核数',
            iowait: '< 10%'
        },
        memory: {
            usage: '< 90%',
            swap: '< 20%',
            cache: '> 20%'
        },
        disk: {
            usage: '< 85%',
            iops: '< 5000/s',
            latency: '< 10ms'
        },
        network: {
            bandwidth: '< 80%',
            connections: '< 10000',
            errors: '< 0.1%'
        }
    };

    // 应用指标
    readonly applicationMetrics = {
        response: {
            p95: '< 500ms',
            p99: '< 1s',
            error: '< 1%'
        },
        throughput: {
            rps: '< 1000',
            concurrent: '< 500'
        },
        resources: {
            threads: '< 200',
            connections: '< 1000'
        }
    };

    // 业务指标
    readonly businessMetrics = {
        users: {
            active: '> 100',
            concurrent: '< 1000'
        },
        operations: {
            success: '> 99%',
            latency: '< 1s'
        }
    };
}
```

### 10.3 故障恢复流程

```typescript
interface RecoveryProcedure {
    // 故障等级定义
    readonly severityLevels = {
        P0: '完全中断',
        P1: '严重影响',
        P2: '部分影响',
        P3: '轻微影响'
    };

    // 响应时间要求
    readonly responseTime = {
        P0: '15分钟内',
        P1: '30分钟内',
        P2: '2小时内',
        P3: '24小时内'
    };

    // 恢复流程
    readonly recoverySteps = {
        identification: [
            '确认故障范围',
            '评估影响程度',
            '确定故障等级'
        ],
        mitigation: [
            '执行应急预案',
            '通知相关人员',
            '记录故障细节'
        ],
        resolution: [
            '定位根本原因',
            '实施解决方案',
            '验证服务恢复'
        ],
        followup: [
            '编写故障报告',
            '更新应急预案',
            '总结改进措施'
        ]
    };
}
```

### 10.4 升级流程

```typescript
interface UpgradeProcedure {
    // 升级前检查
    readonly preUpgradeChecks = [
        '备份所有数据',
        '检查系统资源',
        '准备回滚方案',
        '通知用户升级计划'
    ];

    // 升级步骤
    readonly upgradeSteps = {
        preparation: [
            '停止应用服务',
            '备份配置文件',
            '备份数据库'
        ],
        execution: [
            '更新应用代码',
            '更新配置文件',
            '执行数据库迁移'
        ],
        verification: [
            '启动应用服务',
            '运行健康检查',
            '验证功能正常'
        ]
    };

    // 回滚步骤
    readonly rollbackSteps = {
        decision: [
            '评估故障影响',
            '确认回滚必要性',
            '通知相关人员'
        ],
        execution: [
            '停止新版本',
            '恢复旧版本',
            '恢复数据备份'
        ],
        verification: [
            '验证系统功能',
            '确认数据一致',
            '通知恢复完成'
        ]
    };
}
```

## 11. TypeScript实现

### 11.1 核心模型

```typescript
// 用户模型
interface User {
    id: string;
    username: string;
    email: string;
    password: string;
    status: UserStatus;
    roles: Role[];
    attributes: Map<string, string>;
    createdAt: Date;
    updatedAt: Date;
}

// 角色模型
interface Role {
    id: string;
    name: string;
    description: string;
    permissions: Permission[];
    attributes: Map<string, string>;
    createdAt: Date;
    updatedAt: Date;
}

// 权限模型
interface Permission {
    id: string;
    resourceType: string;
    action: string;
    conditions: Map<string, any>;
    createdAt: Date;
    updatedAt: Date;
}

// 访问策略
interface AccessPolicy {
    id: string;
    name: string;
    description: string;
    effect: 'allow' | 'deny';
    resources: string[];
    actions: string[];
    conditions: Map<string, any>;
    priority: number;
    createdAt: Date;
    updatedAt: Date;
}
```

### 11.2 服务实现

```typescript
@Injectable()
class AuthService {
    constructor(
        private userRepository: UserRepository,
        private roleRepository: RoleRepository,
        private permissionRepository: PermissionRepository,
        private policyRepository: PolicyRepository,
        private cacheManager: CacheManager,
        private logger: LoggerService
    ) {}

    // 用户认证
    async authenticate(credentials: Credentials): Promise<AuthResult> {
        try {
            const user = await this.userRepository.findByUsername(credentials.username);
            if (!user) {
                throw new AuthenticationError('User not found');
            }

            const isValid = await this.verifyPassword(credentials.password, user.password);
            if (!isValid) {
                throw new AuthenticationError('Invalid password');
            }

            const token = await this.generateToken(user);
            return { user, token };
        } catch (error) {
            this.logger.error('Authentication failed', error);
            throw error;
        }
    }

    // 权限检查
    async checkPermission(userId: string, resource: string, action: string): Promise<boolean> {
        try {
            const cachedPermissions = await this.cacheManager.get(`permissions:${userId}`);
            if (cachedPermissions) {
                return this.evaluatePermissions(cachedPermissions, resource, action);
            }

            const user = await this.userRepository.findById(userId);
            const permissions = await this.collectUserPermissions(user);
            await this.cacheManager.set(`permissions:${userId}`, permissions);

            return this.evaluatePermissions(permissions, resource, action);
        } catch (error) {
            this.logger.error('Permission check failed', error);
            return false;
        }
    }

    // 角色管理
    async assignRole(userId: string, roleId: string): Promise<void> {
        try {
            await this.userRepository.addRole(userId, roleId);
            await this.cacheManager.delete(`permissions:${userId}`);
        } catch (error) {
            this.logger.error('Role assignment failed', error);
            throw error;
        }
    }
}
```

### 11.3 工具类

```typescript
// 密码工具
class PasswordUtils {
    static async hash(password: string): Promise<string> {
        const salt = await bcrypt.genSalt(10);
        return bcrypt.hash(password, salt);
    }

    static async verify(password: string, hash: string): Promise<boolean> {
        return bcrypt.compare(password, hash);
    }

    static validateStrength(password: string): ValidationResult {
        const rules = [
            { test: /.{8,}/, message: 'Password must be at least 8 characters' },
            { test: /[A-Z]/, message: 'Password must contain uppercase letters' },
            { test: /[a-z]/, message: 'Password must contain lowercase letters' },
            { test: /[0-9]/, message: 'Password must contain numbers' },
            { test: /[^A-Za-z0-9]/, message: 'Password must contain special characters' }
        ];

        const failures = rules
            .filter(rule => !rule.test.test(password))
            .map(rule => rule.message);

        return {
            valid: failures.length === 0,
            errors: failures
        };
    }
}

// 令牌工具
class TokenUtils {
    static generate(payload: any, options: SignOptions): string {
        return jwt.sign(payload, process.env.JWT_SECRET!, {
            expiresIn: '1h',
            ...options
        });
    }

    static verify(token: string): any {
        try {
            return jwt.verify(token, process.env.JWT_SECRET!);
        } catch (error) {
            throw new TokenVerificationError(error.message);
        }
    }

    static decode(token: string): any {
        return jwt.decode(token);
    }
}
```

### 11.4 中间件

```typescript
// 认证中间件
@Injectable()
class AuthMiddleware implements NestMiddleware {
    constructor(
        private authService: AuthService,
        private logger: LoggerService
    ) {}

    async use(req: Request, res: Response, next: NextFunction) {
        try {
            const token = this.extractToken(req);
            if (!token) {
                throw new UnauthorizedException('No token provided');
            }

            const payload = TokenUtils.verify(token);
            const user = await this.authService.validateUser(payload.sub);
            
            req.user = user;
            next();
        } catch (error) {
            this.logger.error('Authentication middleware failed', error);
            throw new UnauthorizedException('Invalid token');
        }
    }

    private extractToken(req: Request): string | null {
        const authorization = req.headers.authorization;
        if (!authorization) {
            return null;
        }

        const [type, token] = authorization.split(' ');
        return type === 'Bearer' ? token : null;
    }
}

// 权限中间件
@Injectable()
class PermissionMiddleware implements NestMiddleware {
    constructor(
        private authService: AuthService,
        private logger: LoggerService
    ) {}

    async use(req: Request, res: Response, next: NextFunction) {
        try {
            const user = req.user;
            const resource = req.params.resource;
            const action = req.method;

            const hasPermission = await this.authService.checkPermission(
                user.id,
                resource,
                action
            );

            if (!hasPermission) {
                throw new ForbiddenException('Permission denied');
            }

            next();
        } catch (error) {
            this.logger.error('Permission middleware failed', error);
            throw error;
        }
    }
}
```

## 12. 前端实现

### 12.1 组件设计

```typescript
// 登录组件
interface LoginProps {
    onLogin: (credentials: Credentials) => Promise<void>;
    onError: (error: Error) => void;
}

const Login: React.FC<LoginProps> = ({ onLogin, onError }) => {
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [loading, setLoading] = useState(false);

    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();
        setLoading(true);

        try {
            await onLogin({ username, password });
        } catch (error) {
            onError(error);
        } finally {
            setLoading(false);
        }
    };

    return (
        <form onSubmit={handleSubmit}>
            <input
                type="text"
                value={username}
                onChange={e => setUsername(e.target.value)}
                placeholder="Username"
                required
            />
            <input
                type="password"
                value={password}
                onChange={e => setPassword(e.target.value)}
                placeholder="Password"
                required
            />
            <button type="submit" disabled={loading}>
                {loading ? 'Logging in...' : 'Login'}
            </button>
        </form>
    );
};

// 权限控制组件
interface PermissionGuardProps {
    resource: string;
    action: string;
    children: React.ReactNode;
    fallback?: React.ReactNode;
}

const PermissionGuard: React.FC<PermissionGuardProps> = ({
    resource,
    action,
    children,
    fallback
}) => {
    const { checkPermission } = useAuth();
    const [hasPermission, setHasPermission] = useState(false);

    useEffect(() => {
        const check = async () => {
            const result = await checkPermission(resource, action);
            setHasPermission(result);
        };
        check();
    }, [resource, action]);

    return hasPermission ? children : fallback || null;
};
```

### 12.2 状态管理

```typescript
// 认证状态
interface AuthState {
    user: User | null;
    token: string | null;
    loading: boolean;
    error: Error | null;
}

const authSlice = createSlice({
    name: 'auth',
    initialState: {
        user: null,
        token: null,
        loading: false,
        error: null
    } as AuthState,
    reducers: {
        loginStart: state => {
            state.loading = true;
            state.error = null;
        },
        loginSuccess: (state, action) => {
            state.user = action.payload.user;
            state.token = action.payload.token;
            state.loading = false;
            state.error = null;
        },
        loginFailure: (state, action) => {
            state.loading = false;
            state.error = action.payload;
        },
        logout: state => {
            state.user = null;
            state.token = null;
        }
    }
});

// 权限状态
interface PermissionState {
    permissions: Permission[];
    loading: boolean;
    error: Error | null;
}

const permissionSlice = createSlice({
    name: 'permission',
    initialState: {
        permissions: [],
        loading: false,
        error: null
    } as PermissionState,
    reducers: {
        fetchStart: state => {
            state.loading = true;
            state.error = null;
        },
        fetchSuccess: (state, action) => {
            state.permissions = action.payload;
            state.loading = false;
            state.error = null;
        },
        fetchFailure: (state, action) => {
            state.loading = false;
            state.error = action.payload;
        }
    }
});
```

### 12.3 路由设计

```typescript
// 路由配置
const routes: Route[] = [
    {
        path: '/',
        component: Dashboard,
        auth: true,
        permissions: ['dashboard:view']
    },
    {
        path: '/users',
        component: UserManagement,
        auth: true,
        permissions: ['users:manage']
    },
    {
        path: '/roles',
        component: RoleManagement,
        auth: true,
        permissions: ['roles:manage']
    },
    {
        path: '/permissions',
        component: PermissionManagement,
        auth: true,
        permissions: ['permissions:manage']
    },
    {
        path: '/login',
        component: Login,
        auth: false
    }
];

// 路由守卫
const PrivateRoute: React.FC<RouteProps> = ({ component: Component, ...rest }) => {
    const { user, loading } = useAuth();
    const location = useLocation();

    if (loading) {
        return <LoadingSpinner />;
    }

    return (
        <Route
            {...rest}
            render={props =>
                user ? (
                    <Component {...props} />
                ) : (
                    <Redirect
                        to={{
                            pathname: '/login',
                            state: { from: location }
                        }}
                    />
                )
            }
        />
    );
};
```

### 12.4 权限控制

```typescript
// 权限Hook
function usePermission() {
    const { user } = useAuth();
    const dispatch = useDispatch();

    const checkPermission = useCallback(
        (resource: string, action: string) => {
            if (!user) return false;

            return user.permissions.some(
                permission =>
                    permission.resourceType === resource &&
                    permission.action === action
            );
        },
        [user]
    );

    const hasRole = useCallback(
        (roleName: string) => {
            if (!user) return false;
            return user.roles.some(role => role.name === roleName);
        },
        [user]
    );

    return { checkPermission, hasRole };
}

// 权限指令
const withPermission = (
    WrappedComponent: React.ComponentType<any>,
    resource: string,
    action: string
) => {
    return function WithPermissionComponent(props: any) {
        const { checkPermission } = usePermission();
        const hasPermission = checkPermission(resource, action);

        if (!hasPermission) {
            return <Forbidden />;
        }

        return <WrappedComponent {...props} />;
    };
};

// 权限装饰器
function RequirePermission(resource: string, action: string) {
    return function (
        target: any,
        propertyKey: string,
        descriptor: PropertyDescriptor
    ) {
        const originalMethod = descriptor.value;

        descriptor.value = function (...args: any[]) {
            const { checkPermission } = usePermission();
            
            if (!checkPermission(resource, action)) {
                throw new Error('Permission denied');
            }

            return originalMethod.apply(this, args);
        };

        return descriptor;
    };
}
