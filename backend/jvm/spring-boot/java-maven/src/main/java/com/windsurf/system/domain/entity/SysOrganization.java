package com.windsurf.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_organization")
public class SysOrganization extends BaseEntity {
    @TableId
    private Long id;

    private String name;

    private String code;

    private Long parentId;

    private Integer sort;

    private String status;

    private String address;

    private String contact;

    private String phone;

    private String email;

    @TableField(exist = false)
    private List<SysOrganization> children;
}
