package com.windsurf.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_user")
public class SysUser extends BaseEntity {
    @TableId
    private Long id;

    private String username;

    private String password;

    private String nickname;

    private String email;

    private String phone;

    private String avatar;

    private Integer gender;

    private Integer status;

    private Integer sort;

    private Long departmentId;

    private Long organizationId;

    @TableField(exist = false)
    private List<Long> roleIds;

    @TableField(exist = false)
    private String departmentName;

    @TableField(exist = false)
    private List<String> roleNames;
}
