package com.windsurf.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_permission")
public class SysPermission extends BaseEntity {
    @TableId
    private Long id;

    private Long parentId;

    private String name;

    private String code;

    private Integer type;

    private String path;

    private String component;

    private String permission;

    private String icon;

    private Integer sort;

    private Integer status;

    @TableField(exist = false)
    private List<SysPermission> children;

    @TableField(exist = false)
    private String parentName;
}
