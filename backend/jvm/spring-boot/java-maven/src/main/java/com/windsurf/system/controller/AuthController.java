package com.windsurf.system.controller;

import com.windsurf.system.domain.dto.*;
import com.windsurf.system.domain.entity.SysUser;
import com.windsurf.system.exception.Result;
import com.windsurf.system.security.JwtTokenProvider;
import com.windsurf.system.security.encryption.EncryptionManager;
import com.windsurf.system.service.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.servlet.http.HttpSession;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

@Tag(name = "认证管理")
@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {
    
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider tokenProvider;
    private final SysUserService userService;
    private final EncryptionManager encryptionManager;

    @Operation(summary = "获取公钥")
    @GetMapping("/public-key")
    public Result<String> getPublicKey() {
        return Result.ok(encryptionManager.getServerPublicKey());
    }

    @Operation(summary = "登录")
    @PostMapping("/login")
    public Result<EncryptedResponse> login(
            @Valid @RequestBody EncryptedRequest encryptedRequest,
            HttpSession session) throws Exception {
        // 解密请求数据
        LoginRequest loginRequest = encryptionManager.handleEncryptedRequest(
                encryptedRequest, session.getId(), LoginRequest.class);

        // 验证用户名密码
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        loginRequest.getUsername(), 
                        loginRequest.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        String token = tokenProvider.createToken(authentication);

        // 加密响应数据
        return Result.ok(encryptionManager.generateEncryptedResponse(
                new LoginResponse(token), session.getId()));
    }

    @Operation(summary = "登出")
    @PostMapping("/logout")
    public Result<?> logout(HttpSession session) {
        SecurityContextHolder.clearContext();
        encryptionManager.removeSessionKey(session.getId());
        return Result.ok();
    }

    @Operation(summary = "获取当前用户信息")
    @GetMapping("/info")
    public Result<EncryptedResponse> getUserInfo(HttpSession session) throws Exception {
        String username = SecurityContextHolder.getContext().getAuthentication().getName();
        SysUser user = userService.getUserByUsername(username);

        UserInfo userInfo = new UserInfo();
        userInfo.setId(user.getId());
        userInfo.setUsername(user.getUsername());
        userInfo.setNickname(user.getNickname());
        userInfo.setEmail(user.getEmail());
        userInfo.setPhone(user.getPhone());
        userInfo.setAvatar(user.getAvatar());
        userInfo.setDepartmentId(user.getDepartmentId());
        userInfo.setOrganizationId(user.getOrganizationId());
        userInfo.setRoleNames(user.getRoleNames());
        userInfo.setPermissions(userService.getUserPermissions(user.getId()));

        // 加密响应数据
        return Result.ok(encryptionManager.generateEncryptedResponse(
                userInfo, session.getId()));
    }
}
