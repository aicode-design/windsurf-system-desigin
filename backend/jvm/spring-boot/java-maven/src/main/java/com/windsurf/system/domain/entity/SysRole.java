package com.windsurf.system.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = true)
@TableName("sys_role")
public class SysRole extends BaseEntity {
    @TableId
    private Long id;

    private String name;

    private String code;

    private String description;

    private Integer sort;

    private Integer status;

    @TableField(exist = false)
    private List<Long> permissionIds;

    @TableField(exist = false)
    private List<String> permissionNames;
}
