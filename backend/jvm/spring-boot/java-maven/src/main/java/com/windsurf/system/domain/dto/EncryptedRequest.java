package com.windsurf.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "加密请求")
public class EncryptedRequest {
    
    @Schema(description = "AES密钥(RSA加密后的)")
    private String encryptedKey;
    
    @Schema(description = "数据(AES加密后的)")
    private String encryptedData;
}
