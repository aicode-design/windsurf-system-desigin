package com.windsurf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.windsurf.system.domain.entity.SysDepartment;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysDepartmentMapper extends BaseMapper<SysDepartment> {
}
