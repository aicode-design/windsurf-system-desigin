package com.windsurf.system.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.windsurf.system.domain.entity.SysUser;

public interface SysUserService extends IService<SysUser> {
    Page<SysUser> getUserPage(Page<SysUser> page, String keyword);

    SysUser getUserByUsername(String username);

    void createUser(SysUser user);

    void updateUser(SysUser user);

    void deleteUser(Long id);

    void resetPassword(Long id, String password);

    void updateStatus(Long id, String status);
}
