package com.windsurf.system.controller;

import com.windsurf.system.domain.entity.SysDepartment;
import com.windsurf.system.service.SysDepartmentService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "部门管理")
@RestController
@RequestMapping("/system/department")
@RequiredArgsConstructor
public class SysDepartmentController {
    private final SysDepartmentService departmentService;

    @Operation(summary = "获取部门树")
    @GetMapping("/tree")
    @PreAuthorize("hasAuthority('system:department:list')")
    public List<SysDepartment> getDepartmentTree(
            @Parameter(description = "组织ID") @RequestParam Long organizationId) {
        return departmentService.getDepartmentTree(organizationId);
    }

    @Operation(summary = "获取部门信息")
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:department:query')")
    public SysDepartment getDepartment(@Parameter(description = "部门ID") @PathVariable Long id) {
        return departmentService.getById(id);
    }

    @Operation(summary = "创建部门")
    @PostMapping
    @PreAuthorize("hasAuthority('system:department:add')")
    public void createDepartment(@RequestBody SysDepartment department) {
        departmentService.createDepartment(department);
    }

    @Operation(summary = "更新部门")
    @PutMapping
    @PreAuthorize("hasAuthority('system:department:edit')")
    public void updateDepartment(@RequestBody SysDepartment department) {
        departmentService.updateDepartment(department);
    }

    @Operation(summary = "删除部门")
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('system:department:delete')")
    public void deleteDepartment(@Parameter(description = "部门ID") @PathVariable Long id) {
        departmentService.deleteDepartment(id);
    }

    @Operation(summary = "更新状态")
    @PutMapping("/{id}/status")
    @PreAuthorize("hasAuthority('system:department:edit')")
    public void updateStatus(
            @Parameter(description = "部门ID") @PathVariable Long id,
            @Parameter(description = "状态") @RequestParam String status) {
        departmentService.updateStatus(id, status);
    }
}
