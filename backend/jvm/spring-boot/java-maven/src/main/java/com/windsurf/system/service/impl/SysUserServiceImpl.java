package com.windsurf.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.windsurf.system.domain.entity.SysUser;
import com.windsurf.system.mapper.SysUserMapper;
import com.windsurf.system.service.SysUserService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

@Service
@RequiredArgsConstructor
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    private final PasswordEncoder passwordEncoder;

    @Override
    public Page<SysUser> getUserPage(Page<SysUser> page, String keyword) {
        LambdaQueryWrapper<SysUser> wrapper = new LambdaQueryWrapper<>();
        if (StringUtils.hasText(keyword)) {
            wrapper.like(SysUser::getUsername, keyword)
                    .or()
                    .like(SysUser::getEmail, keyword)
                    .or()
                    .like(SysUser::getNickname, keyword);
        }
        wrapper.orderByDesc(SysUser::getCreateTime);
        return page(page, wrapper);
    }

    @Override
    public SysUser getUserByUsername(String username) {
        return baseMapper.selectByUsername(username);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createUser(SysUser user) {
        // Check username uniqueness
        if (isUsernameExists(user.getUsername())) {
            throw new ServiceException("用户名已存在");
        }
        
        // Check email uniqueness if provided
        if (StringUtils.hasText(user.getEmail()) && isEmailExists(user.getEmail())) {
            throw new ServiceException("邮箱已存在");
        }
        
        // Encode password
        user.setPassword(passwordEncoder.encode(user.getPassword()));
        
        // Save user
        save(user);
        
        // Save user roles if provided
        if (user.getRoleIds() != null && !user.getRoleIds().isEmpty()) {
            baseMapper.insertUserRoles(user.getId(), user.getRoleIds());
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateUser(SysUser user) {
        // Check username uniqueness
        SysUser existingUser = baseMapper.selectById(user.getId());
        if (!existingUser.getUsername().equals(user.getUsername()) && isUsernameExists(user.getUsername())) {
            throw new ServiceException("用户名已存在");
        }
        
        // Check email uniqueness if provided
        if (StringUtils.hasText(user.getEmail()) && !user.getEmail().equals(existingUser.getEmail()) 
            && isEmailExists(user.getEmail())) {
            throw new ServiceException("邮箱已存在");
        }
        
        // If password is provided, encode it
        if (StringUtils.hasText(user.getPassword())) {
            user.setPassword(passwordEncoder.encode(user.getPassword()));
        } else {
            user.setPassword(null); // Don't update password if not provided
        }
        
        // Update user
        updateById(user);
        
        // Update user roles if provided
        if (user.getRoleIds() != null) {
            baseMapper.deleteUserRoles(user.getId());
            if (!user.getRoleIds().isEmpty()) {
                baseMapper.insertUserRoles(user.getId(), user.getRoleIds());
            }
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteUser(Long id) {
        // Delete user roles first
        baseMapper.deleteUserRoles(id);
        // Then delete user
        removeById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void resetPassword(Long id, String password) {
        SysUser user = new SysUser();
        user.setId(id);
        user.setPassword(passwordEncoder.encode(password));
        updateById(user);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStatus(Long id, String status) {
        SysUser user = new SysUser();
        user.setId(id);
        user.setStatus("正常".equals(status) ? 1 : 0);
        updateById(user);
    }

    private boolean isUsernameExists(String username) {
        return baseMapper.selectCount(new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getUsername, username)) > 0;
    }

    private boolean isEmailExists(String email) {
        return baseMapper.selectCount(new LambdaQueryWrapper<SysUser>()
                .eq(SysUser::getEmail, email)) > 0;
    }
}
