package com.windsurf.system.service.impl;

import com.windsurf.system.domain.excel.UserExcel;
import com.windsurf.system.domain.excel.RoleExcel;
import com.windsurf.system.domain.excel.PermissionExcel;
import com.windsurf.system.domain.entity.SysUser;
import com.windsurf.system.domain.entity.SysRole;
import com.windsurf.system.domain.entity.SysPermission;
import com.windsurf.system.service.*;
import com.windsurf.system.utils.ExcelUtils;
import com.windsurf.system.utils.ExcelConvertUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class ExcelServiceImpl implements ExcelService {

    private final ExcelUtils excelUtils;
    private final ExcelConvertUtils excelConvertUtils;
    private final SysUserService userService;
    private final SysRoleService roleService;
    private final SysPermissionService permissionService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void importUser(MultipartFile file) {
        excelUtils.readExcel(file, UserExcel.class, dataList -> {
            List<SysUser> users = dataList.stream()
                    .map(excelConvertUtils::convertToUser)
                    .collect(Collectors.toList());
            userService.saveBatch(users);
        });
    }

    @Override
    public Resource exportUser() {
        List<SysUser> users = userService.list();
        List<UserExcel> excelList = users.stream()
                .map(excelConvertUtils::convertFromUser)
                .collect(Collectors.toList());
        return excelUtils.generateExcel(excelList, UserExcel.class);
    }

    @Override
    public Resource getUserTemplate() {
        return excelUtils.generateTemplate(UserExcel.class);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void importRole(MultipartFile file) {
        excelUtils.readExcel(file, RoleExcel.class, dataList -> {
            List<SysRole> roles = dataList.stream()
                    .map(excelConvertUtils::convertToRole)
                    .collect(Collectors.toList());
            roleService.saveBatch(roles);
        });
    }

    @Override
    public Resource exportRole() {
        List<SysRole> roles = roleService.list();
        List<RoleExcel> excelList = roles.stream()
                .map(excelConvertUtils::convertFromRole)
                .collect(Collectors.toList());
        return excelUtils.generateExcel(excelList, RoleExcel.class);
    }

    @Override
    public Resource getRoleTemplate() {
        return excelUtils.generateTemplate(RoleExcel.class);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void importPermission(MultipartFile file) {
        excelUtils.readExcel(file, PermissionExcel.class, dataList -> {
            List<SysPermission> permissions = dataList.stream()
                    .map(excelConvertUtils::convertToPermission)
                    .collect(Collectors.toList());
            permissionService.saveBatch(permissions);
        });
    }

    @Override
    public Resource exportPermission() {
        List<SysPermission> permissions = permissionService.list();
        List<PermissionExcel> excelList = permissions.stream()
                .map(excelConvertUtils::convertFromPermission)
                .collect(Collectors.toList());
        return excelUtils.generateExcel(excelList, PermissionExcel.class);
    }

    @Override
    public Resource getPermissionTemplate() {
        return excelUtils.generateTemplate(PermissionExcel.class);
    }
}
