package com.windsurf.system.domain.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;

@Data
public class OrganizationExcel {
    @ExcelProperty(value = "上级组织编码")
    private String parentCode;

    @ExcelProperty(value = "组织名称")
    private String name;

    @ExcelProperty(value = "组织编码")
    private String code;

    @ExcelProperty(value = "排序")
    private Integer sort;

    @ExcelProperty(value = "状态")
    private String status;

    @ExcelProperty(value = "地址")
    private String address;

    @ExcelProperty(value = "联系人")
    private String contact;

    @ExcelProperty(value = "电话")
    private String phone;

    @ExcelProperty(value = "邮箱")
    private String email;
}
