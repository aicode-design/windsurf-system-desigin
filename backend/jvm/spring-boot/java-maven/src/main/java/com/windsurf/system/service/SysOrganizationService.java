package com.windsurf.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.windsurf.system.domain.entity.SysOrganization;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface SysOrganizationService extends IService<SysOrganization> {
    List<SysOrganization> getOrganizationTree();

    void createOrganization(SysOrganization organization);

    void updateOrganization(SysOrganization organization);

    void deleteOrganization(Long id);

    void updateStatus(Long id, String status);

    Resource generateImportTemplate();

    void importData(MultipartFile file);

    Resource exportData();

    void batchUpdateStatus(List<Long> ids, String status);

    void batchDelete(List<Long> ids);

    void updateSort(Long draggedId, Long droppedId);
}
