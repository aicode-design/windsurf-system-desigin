package com.windsurf.system.controller;

import com.windsurf.system.domain.entity.SysOrganization;
import com.windsurf.system.service.SysOrganizationService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Tag(name = "组织管理")
@RestController
@RequestMapping("/system/organization")
@RequiredArgsConstructor
public class SysOrganizationController {
    private final SysOrganizationService organizationService;

    @Operation(summary = "获取组织树")
    @GetMapping("/tree")
    @PreAuthorize("hasAuthority('system:organization:list')")
    public List<SysOrganization> getOrganizationTree() {
        return organizationService.getOrganizationTree();
    }

    @Operation(summary = "创建组织")
    @PostMapping
    @PreAuthorize("hasAuthority('system:organization:add')")
    public void createOrganization(@RequestBody SysOrganization organization) {
        organizationService.createOrganization(organization);
    }

    @Operation(summary = "获取组织信息")
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:organization:query')")
    public SysOrganization getOrganization(@Parameter(description = "组织ID") @PathVariable Long id) {
        return organizationService.getById(id);
    }

    @Operation(summary = "更新组织")
    @PutMapping
    @PreAuthorize("hasAuthority('system:organization:edit')")
    public void updateOrganization(@RequestBody SysOrganization organization) {
        organizationService.updateOrganization(organization);
    }

    @Operation(summary = "删除组织")
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('system:organization:delete')")
    public void deleteOrganization(@Parameter(description = "组织ID") @PathVariable Long id) {
        organizationService.deleteOrganization(id);
    }

    @Operation(summary = "更新状态")
    @PutMapping("/{id}/status")
    @PreAuthorize("hasAuthority('system:organization:edit')")
    public void updateStatus(
            @Parameter(description = "组织ID") @PathVariable Long id,
            @Parameter(description = "状态") @RequestParam String status) {
        organizationService.updateStatus(id, status);
    }

    @Operation(summary = "下载导入模板")
    @GetMapping("/template")
    @PreAuthorize("hasAuthority('system:organization:import')")
    public ResponseEntity<Resource> downloadTemplate() {
        Resource template = organizationService.generateImportTemplate();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=organization_template.xlsx")
                .body(template);
    }

    @Operation(summary = "导入组织数据")
    @PostMapping("/import")
    @PreAuthorize("hasAuthority('system:organization:import')")
    public void importData(@RequestParam("file") MultipartFile file) {
        organizationService.importData(file);
    }

    @Operation(summary = "导出组织数据")
    @GetMapping("/export")
    @PreAuthorize("hasAuthority('system:organization:export')")
    public ResponseEntity<Resource> exportData() {
        Resource data = organizationService.exportData();
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=organization_data.xlsx")
                .body(data);
    }

    @Operation(summary = "批量启用")
    @PostMapping("/batch/enable")
    @PreAuthorize("hasAuthority('system:organization:edit')")
    public void batchEnable(@RequestBody List<Long> ids) {
        organizationService.batchUpdateStatus(ids, "active");
    }

    @Operation(summary = "批量禁用")
    @PostMapping("/batch/disable")
    @PreAuthorize("hasAuthority('system:organization:edit')")
    public void batchDisable(@RequestBody List<Long> ids) {
        organizationService.batchUpdateStatus(ids, "inactive");
    }

    @Operation(summary = "批量删除")
    @DeleteMapping("/batch")
    @PreAuthorize("hasAuthority('system:organization:delete')")
    public void batchDelete(@RequestBody List<Long> ids) {
        organizationService.batchDelete(ids);
    }

    @Operation(summary = "更新排序")
    @PostMapping("/sort")
    @PreAuthorize("hasAuthority('system:organization:edit')")
    public void updateSort(@RequestBody SortRequest request) {
        organizationService.updateSort(request.getDraggedId(), request.getDroppedId());
    }
}

class SortRequest {
    private Long draggedId;
    private Long droppedId;

    public Long getDraggedId() {
        return draggedId;
    }

    public void setDraggedId(Long draggedId) {
        this.draggedId = draggedId;
    }

    public Long getDroppedId() {
        return droppedId;
    }

    public void setDroppedId(Long droppedId) {
        this.droppedId = droppedId;
    }
}
