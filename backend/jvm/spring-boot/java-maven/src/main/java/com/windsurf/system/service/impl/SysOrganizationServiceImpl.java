package com.windsurf.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.windsurf.system.domain.entity.SysOrganization;
import com.windsurf.system.mapper.SysOrganizationMapper;
import com.windsurf.system.service.SysOrganizationService;
import com.windsurf.system.utils.ExcelUtils;
import lombok.RequiredArgsConstructor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class SysOrganizationServiceImpl extends ServiceImpl<SysOrganizationMapper, SysOrganization> implements SysOrganizationService {
    private final ExcelUtils excelUtils;

    @Override
    public List<SysOrganization> getOrganizationTree() {
        LambdaQueryWrapper<SysOrganization> wrapper = new LambdaQueryWrapper<>();
        wrapper.orderByAsc(SysOrganization::getSort);
        
        List<SysOrganization> organizations = list(wrapper);
        return buildTree(organizations);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createOrganization(SysOrganization organization) {
        // 设置默认值
        if (organization.getSort() == null) {
            organization.setSort(0);
        }
        if (organization.getStatus() == null) {
            organization.setStatus("active");
        }
        save(organization);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateOrganization(SysOrganization organization) {
        updateById(organization);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteOrganization(Long id) {
        // 检查是否有子组织
        LambdaQueryWrapper<SysOrganization> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysOrganization::getParentId, id);
        if (count(wrapper) > 0) {
            throw new RuntimeException("存在子组织，无法删除");
        }
        removeById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStatus(Long id, String status) {
        SysOrganization organization = new SysOrganization();
        organization.setId(id);
        organization.setStatus(status);
        updateById(organization);
    }

    @Override
    public Resource generateImportTemplate() {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("组织导入模板");
            
            // 创建表头
            Row headerRow = sheet.createRow(0);
            String[] headers = {"上级组织编码", "组织名称*", "组织编码*", "排序", "状态", "地址", "联系人", "电话", "邮箱"};
            for (int i = 0; i < headers.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(headers[i]);
            }

            // 设置列宽
            for (int i = 0; i < headers.length; i++) {
                sheet.setColumnWidth(i, 15 * 256);
            }

            // 写入到字节数组
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayResource(outputStream.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("生成模板失败", e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void importData(MultipartFile file) {
        try {
            List<SysOrganization> organizations = excelUtils.readExcel(file, SysOrganization.class);
            
            // 获取所有组织编码映射
            Map<String, Long> codeToIdMap = list().stream()
                    .collect(Collectors.toMap(SysOrganization::getCode, SysOrganization::getId));

            // 处理父组织关系
            organizations.forEach(org -> {
                if (org.getParentId() != null) {
                    Long parentId = codeToIdMap.get(org.getCode());
                    if (parentId == null) {
                        throw new RuntimeException("上级组织编码不存在：" + org.getCode());
                    }
                    org.setParentId(parentId);
                }
            });

            // 批量保存
            saveBatch(organizations);
        } catch (Exception e) {
            throw new RuntimeException("导入数据失败", e);
        }
    }

    @Override
    public Resource exportData() {
        try (Workbook workbook = new XSSFWorkbook()) {
            Sheet sheet = workbook.createSheet("组织数据");
            
            // 创建表头
            Row headerRow = sheet.createRow(0);
            String[] headers = {"组织名称", "组织编码", "上级组织", "排序", "状态", "地址", "联系人", "电话", "邮箱"};
            for (int i = 0; i < headers.length; i++) {
                Cell cell = headerRow.createCell(i);
                cell.setCellValue(headers[i]);
            }

            // 获取所有组织数据
            List<SysOrganization> organizations = list();
            Map<Long, String> idToNameMap = organizations.stream()
                    .collect(Collectors.toMap(SysOrganization::getId, SysOrganization::getName));

            // 写入数据
            int rowNum = 1;
            for (SysOrganization org : organizations) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(org.getName());
                row.createCell(1).setCellValue(org.getCode());
                row.createCell(2).setCellValue(org.getParentId() != null ? idToNameMap.get(org.getParentId()) : "");
                row.createCell(3).setCellValue(org.getSort());
                row.createCell(4).setCellValue(org.getStatus());
                row.createCell(5).setCellValue(org.getAddress());
                row.createCell(6).setCellValue(org.getContact());
                row.createCell(7).setCellValue(org.getPhone());
                row.createCell(8).setCellValue(org.getEmail());
            }

            // 设置列宽
            for (int i = 0; i < headers.length; i++) {
                sheet.setColumnWidth(i, 15 * 256);
            }

            // 写入到字节数组
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            workbook.write(outputStream);
            return new ByteArrayResource(outputStream.toByteArray());
        } catch (IOException e) {
            throw new RuntimeException("导出数据失败", e);
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchUpdateStatus(List<Long> ids, String status) {
        List<SysOrganization> organizations = ids.stream().map(id -> {
            SysOrganization org = new SysOrganization();
            org.setId(id);
            org.setStatus(status);
            return org;
        }).collect(Collectors.toList());
        updateBatchById(organizations);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void batchDelete(List<Long> ids) {
        // 检查是否有子组织
        LambdaQueryWrapper<SysOrganization> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(SysOrganization::getParentId, ids);
        if (count(wrapper) > 0) {
            throw new RuntimeException("选中的组织中存在子组织，无法删除");
        }
        removeBatchByIds(ids);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateSort(Long draggedId, Long droppedId) {
        // 获取被拖拽的组织和目标组织
        SysOrganization draggedOrg = getById(draggedId);
        SysOrganization droppedOrg = getById(droppedId);
        if (draggedOrg == null || droppedOrg == null) {
            throw new RuntimeException("组织不存在");
        }

        // 更新父级和排序
        draggedOrg.setParentId(droppedOrg.getParentId());
        draggedOrg.setSort(droppedOrg.getSort() + 1);

        // 更新同级其他组织的排序
        LambdaQueryWrapper<SysOrganization> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysOrganization::getParentId, droppedOrg.getParentId())
                .ge(SysOrganization::getSort, droppedOrg.getSort() + 1)
                .ne(SysOrganization::getId, draggedId);
        
        List<SysOrganization> needUpdateOrgs = list(wrapper);
        if (!needUpdateOrgs.isEmpty()) {
            needUpdateOrgs.forEach(org -> org.setSort(org.getSort() + 1));
            updateBatchById(needUpdateOrgs);
        }

        // 更新拖拽的组织
        updateById(draggedOrg);
    }

    private List<SysOrganization> buildTree(List<SysOrganization> organizations) {
        Map<Long, List<SysOrganization>> childrenMap = organizations.stream()
                .filter(org -> org.getParentId() != null)
                .collect(Collectors.groupingBy(SysOrganization::getParentId));

        organizations.forEach(org -> org.setChildren(childrenMap.getOrDefault(org.getId(), new ArrayList<>())));

        return organizations.stream()
                .filter(org -> org.getParentId() == null)
                .collect(Collectors.toList());
    }
}
