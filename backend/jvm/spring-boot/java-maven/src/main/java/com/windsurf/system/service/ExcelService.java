package com.windsurf.system.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

public interface ExcelService {
    /**
     * 导入用户数据
     */
    void importUser(MultipartFile file);

    /**
     * 导出用户数据
     */
    Resource exportUser();

    /**
     * 获取用户导入模板
     */
    Resource getUserTemplate();

    /**
     * 导入角色数据
     */
    void importRole(MultipartFile file);

    /**
     * 导出角色数据
     */
    Resource exportRole();

    /**
     * 获取角色导入模板
     */
    Resource getRoleTemplate();

    /**
     * 导入权限数据
     */
    void importPermission(MultipartFile file);

    /**
     * 导出权限数据
     */
    Resource exportPermission();

    /**
     * 获取权限导入模板
     */
    Resource getPermissionTemplate();
}
