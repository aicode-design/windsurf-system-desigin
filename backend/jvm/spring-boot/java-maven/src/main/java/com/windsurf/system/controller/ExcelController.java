package com.windsurf.system.controller;

import com.windsurf.system.common.response.R;
import com.windsurf.system.service.ExcelService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/api/system")
@RequiredArgsConstructor
@Api(tags = "Excel导入导出接口")
public class ExcelController {

    private final ExcelService excelService;

    @PostMapping("/user/import")
    @PreAuthorize("@ss.hasPermission('system:user:import')")
    @ApiOperation("导入用户数据")
    public R<?> importUser(@RequestParam("file") MultipartFile file) {
        excelService.importUser(file);
        return R.ok();
    }

    @GetMapping("/user/export")
    @PreAuthorize("@ss.hasPermission('system:user:export')")
    @ApiOperation("导出用户数据")
    public ResponseEntity<Resource> exportUser() {
        Resource resource = excelService.exportUser();
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=users.xlsx")
                .body(resource);
    }

    @GetMapping("/user/template")
    @PreAuthorize("@ss.hasPermission('system:user:import')")
    @ApiOperation("下载用户导入模板")
    public ResponseEntity<Resource> downloadUserTemplate() {
        Resource resource = excelService.getUserTemplate();
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=user_template.xlsx")
                .body(resource);
    }

    @PostMapping("/role/import")
    @PreAuthorize("@ss.hasPermission('system:role:import')")
    @ApiOperation("导入角色数据")
    public R<?> importRole(@RequestParam("file") MultipartFile file) {
        excelService.importRole(file);
        return R.ok();
    }

    @GetMapping("/role/export")
    @PreAuthorize("@ss.hasPermission('system:role:export')")
    @ApiOperation("导出角色数据")
    public ResponseEntity<Resource> exportRole() {
        Resource resource = excelService.exportRole();
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=roles.xlsx")
                .body(resource);
    }

    @GetMapping("/role/template")
    @PreAuthorize("@ss.hasPermission('system:role:import')")
    @ApiOperation("下载角色导入模板")
    public ResponseEntity<Resource> downloadRoleTemplate() {
        Resource resource = excelService.getRoleTemplate();
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=role_template.xlsx")
                .body(resource);
    }

    @PostMapping("/permission/import")
    @PreAuthorize("@ss.hasPermission('system:permission:import')")
    @ApiOperation("导入权限数据")
    public R<?> importPermission(@RequestParam("file") MultipartFile file) {
        excelService.importPermission(file);
        return R.ok();
    }

    @GetMapping("/permission/export")
    @PreAuthorize("@ss.hasPermission('system:permission:export')")
    @ApiOperation("导出权限数据")
    public ResponseEntity<Resource> exportPermission() {
        Resource resource = excelService.exportPermission();
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=permissions.xlsx")
                .body(resource);
    }

    @GetMapping("/permission/template")
    @PreAuthorize("@ss.hasPermission('system:permission:import')")
    @ApiOperation("下载权限导入模板")
    public ResponseEntity<Resource> downloadPermissionTemplate() {
        Resource resource = excelService.getPermissionTemplate();
        return ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/vnd.ms-excel"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=permission_template.xlsx")
                .body(resource);
    }
}
