package com.windsurf.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.windsurf.system.domain.entity.SysDepartment;

import java.util.List;

public interface SysDepartmentService extends IService<SysDepartment> {
    List<SysDepartment> getDepartmentTree(Long organizationId);

    void createDepartment(SysDepartment department);

    void updateDepartment(SysDepartment department);

    void deleteDepartment(Long id);

    void updateStatus(Long id, String status);
}
