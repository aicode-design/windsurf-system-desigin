package com.windsurf.system.domain.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(description = "加密响应")
public class EncryptedResponse {
    
    @Schema(description = "数据(AES加密后的)")
    private String encryptedData;
    
    public static EncryptedResponse of(String encryptedData) {
        EncryptedResponse response = new EncryptedResponse();
        response.setEncryptedData(encryptedData);
        return response;
    }
}
