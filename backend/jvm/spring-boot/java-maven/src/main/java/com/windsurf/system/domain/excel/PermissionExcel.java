package com.windsurf.system.domain.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import java.util.Date;

@Data
public class PermissionExcel {
    @ExcelProperty(value = "上级权限")
    private String parentName;

    @ExcelProperty(value = "权限名称")
    private String name;

    @ExcelProperty(value = "权限编码")
    private String code;

    @ExcelProperty(value = "权限类型")
    private String type;

    @ExcelProperty(value = "菜单路径")
    private String path;

    @ExcelProperty(value = "组件路径")
    private String component;

    @ExcelProperty(value = "权限标识")
    private String permission;

    @ExcelProperty(value = "图标")
    private String icon;

    @ExcelProperty(value = "状态")
    private String status;

    @ExcelProperty(value = "排序")
    private Integer sort;

    @ExcelProperty(value = "创建时间")
    private Date createTime;
}
