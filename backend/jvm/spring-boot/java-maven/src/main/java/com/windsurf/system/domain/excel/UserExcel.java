package com.windsurf.system.domain.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import java.util.Date;

@Data
public class UserExcel {
    @ExcelProperty(value = "用户名")
    private String username;

    @ExcelProperty(value = "昵称")
    private String nickname;
    
    @ExcelProperty(value = "部门")
    private String deptName;

    @ExcelProperty(value = "角色")
    private String roleNames;

    @ExcelProperty(value = "手机号")
    private String phone;

    @ExcelProperty(value = "邮箱")
    private String email;

    @ExcelProperty(value = "性别")
    private String gender;

    @ExcelProperty(value = "状态")
    private String status;

    @ExcelProperty(value = "排序")
    private Integer sort;

    @ExcelProperty(value = "创建时间")
    private Date createTime;
}
