package com.windsurf.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.windsurf.system.domain.entity.SysOrganization;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SysOrganizationMapper extends BaseMapper<SysOrganization> {
}
