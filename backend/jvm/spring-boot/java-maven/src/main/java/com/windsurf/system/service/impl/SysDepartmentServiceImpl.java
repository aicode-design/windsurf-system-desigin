package com.windsurf.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.windsurf.system.domain.entity.SysDepartment;
import com.windsurf.system.mapper.SysDepartmentMapper;
import com.windsurf.system.service.SysDepartmentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class SysDepartmentServiceImpl extends ServiceImpl<SysDepartmentMapper, SysDepartment> implements SysDepartmentService {

    @Override
    public List<SysDepartment> getDepartmentTree(Long organizationId) {
        LambdaQueryWrapper<SysDepartment> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(SysDepartment::getOrganizationId, organizationId)
                .orderByAsc(SysDepartment::getSort);
        
        List<SysDepartment> departments = list(wrapper);
        return buildTree(departments);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void createDepartment(SysDepartment department) {
        save(department);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateDepartment(SysDepartment department) {
        updateById(department);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void deleteDepartment(Long id) {
        removeById(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void updateStatus(Long id, String status) {
        SysDepartment department = new SysDepartment();
        department.setId(id);
        department.setStatus(status);
        updateById(department);
    }

    private List<SysDepartment> buildTree(List<SysDepartment> departments) {
        Map<Long, List<SysDepartment>> childrenMap = departments.stream()
                .filter(dept -> dept.getParentId() != null)
                .collect(Collectors.groupingBy(SysDepartment::getParentId));

        departments.forEach(dept -> dept.setChildren(childrenMap.getOrDefault(dept.getId(), new ArrayList<>())));

        return departments.stream()
                .filter(dept -> dept.getParentId() == null)
                .collect(Collectors.toList());
    }
}
