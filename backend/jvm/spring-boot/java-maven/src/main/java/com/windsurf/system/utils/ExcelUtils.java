package com.windsurf.system.utils;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.read.listener.ReadListener;
import com.alibaba.excel.write.metadata.style.WriteCellStyle;
import com.alibaba.excel.write.style.HorizontalCellStyleStrategy;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@Slf4j
@Component
public class ExcelUtils {

    /**
     * 读取Excel文件
     *
     * @param file     Excel文件
     * @param clazz    数据类型
     * @param consumer 数据处理函数
     * @param <T>      数据类型
     */
    public <T> void readExcel(MultipartFile file, Class<T> clazz, Consumer<List<T>> consumer) {
        try {
            EasyExcel.read(file.getInputStream(), clazz, new ReadListener<T>() {
                private final List<T> dataList = new ArrayList<>();

                @Override
                public void invoke(T data, AnalysisContext context) {
                    dataList.add(data);
                    // 每1000条处理一次，防止数据量过大导致内存溢出
                    if (dataList.size() >= 1000) {
                        consumer.accept(dataList);
                        dataList.clear();
                    }
                }

                @Override
                public void doAfterAllAnalysed(AnalysisContext context) {
                    if (!dataList.isEmpty()) {
                        consumer.accept(dataList);
                        dataList.clear();
                    }
                }
            }).sheet().doRead();
        } catch (Exception e) {
            log.error("Read excel error", e);
            throw new RuntimeException("导入Excel失败", e);
        }
    }

    /**
     * 生成Excel文件
     *
     * @param data  数据列表
     * @param clazz 数据类型
     * @param <T>   数据类型
     * @return Excel文件的Resource
     */
    public <T> Resource generateExcel(List<T> data, Class<T> clazz) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            
            // 设置样式
            WriteCellStyle headWriteCellStyle = new WriteCellStyle();
            headWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
            WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
            contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.LEFT);
            HorizontalCellStyleStrategy styleStrategy = new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);

            // 写入数据
            EasyExcel.write(outputStream, clazz)
                    .registerWriteHandler(styleStrategy)
                    .sheet("数据")
                    .doWrite(data);

            return new ByteArrayResource(outputStream.toByteArray());
        } catch (Exception e) {
            log.error("Generate excel error", e);
            throw new RuntimeException("生成Excel失败", e);
        }
    }

    /**
     * 生成Excel模板
     *
     * @param clazz 数据类型
     * @param <T>   数据类型
     * @return Excel模板的Resource
     */
    public <T> Resource generateTemplate(Class<T> clazz) {
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            
            // 设置样式
            WriteCellStyle headWriteCellStyle = new WriteCellStyle();
            headWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.CENTER);
            WriteCellStyle contentWriteCellStyle = new WriteCellStyle();
            contentWriteCellStyle.setHorizontalAlignment(HorizontalAlignment.LEFT);
            HorizontalCellStyleStrategy styleStrategy = new HorizontalCellStyleStrategy(headWriteCellStyle, contentWriteCellStyle);

            // 写入模板
            EasyExcel.write(outputStream, clazz)
                    .registerWriteHandler(styleStrategy)
                    .sheet("模板")
                    .doWrite(new ArrayList<>());

            return new ByteArrayResource(outputStream.toByteArray());
        } catch (Exception e) {
            log.error("Generate template error", e);
            throw new RuntimeException("生成模板失败", e);
        }
    }
}
