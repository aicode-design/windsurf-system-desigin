package com.windsurf.system;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WindsurfSystemApplication {
    public static void main(String[] args) {
        SpringApplication.run(WindsurfSystemApplication.class, args);
    }
}
