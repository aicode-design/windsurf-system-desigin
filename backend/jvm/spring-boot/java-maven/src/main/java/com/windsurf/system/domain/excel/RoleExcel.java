package com.windsurf.system.domain.excel;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.Data;
import java.util.Date;

@Data
public class RoleExcel {
    @ExcelProperty(value = "角色名称")
    private String name;

    @ExcelProperty(value = "角色编码")
    private String code;

    @ExcelProperty(value = "权限")
    private String permissionNames;

    @ExcelProperty(value = "描述")
    private String description;

    @ExcelProperty(value = "状态")
    private String status;

    @ExcelProperty(value = "排序")
    private Integer sort;

    @ExcelProperty(value = "创建时间")
    private Date createTime;
}
