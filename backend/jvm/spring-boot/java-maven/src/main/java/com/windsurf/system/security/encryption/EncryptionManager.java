package com.windsurf.system.security.encryption;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.windsurf.system.domain.dto.EncryptedRequest;
import com.windsurf.system.domain.dto.EncryptedResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.crypto.SecretKey;
import java.security.KeyPair;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
@RequiredArgsConstructor
public class EncryptionManager {
    
    private final EncryptionUtils encryptionUtils;
    private final ObjectMapper objectMapper;
    
    private KeyPair serverKeyPair;
    private final Map<String, String> sessionKeyMap = new ConcurrentHashMap<>();
    
    @PostConstruct
    public void init() throws Exception {
        // 生成服务器RSA密钥对
        serverKeyPair = encryptionUtils.generateRSAKeyPair();
    }
    
    /**
     * 获取服务器公钥
     */
    public String getServerPublicKey() {
        return encryptionUtils.keyToBase64(serverKeyPair.getPublic());
    }
    
    /**
     * 处理加密请求
     */
    public <T> T handleEncryptedRequest(EncryptedRequest request, String sessionId, Class<T> type) throws Exception {
        // 使用服务器私钥解密AES密钥
        String aesKey = encryptionUtils.decryptRSA(request.getEncryptedKey(), 
                encryptionUtils.keyToBase64(serverKeyPair.getPrivate()));
        
        // 存储会话密钥
        sessionKeyMap.put(sessionId, aesKey);
        
        // 使用AES密钥解密数据
        String decryptedData = encryptionUtils.decryptAES(request.getEncryptedData(), aesKey);
        
        // 将解密后的数据转换为对象
        return objectMapper.readValue(decryptedData, type);
    }
    
    /**
     * 生成加密响应
     */
    public EncryptedResponse generateEncryptedResponse(Object data, String sessionId) throws Exception {
        // 获取会话密钥
        String aesKey = sessionKeyMap.get(sessionId);
        if (aesKey == null) {
            throw new IllegalStateException("Session key not found");
        }
        
        // 将数据转换为JSON字符串
        String jsonData = objectMapper.writeValueAsString(data);
        
        // 使用AES密钥加密数据
        String encryptedData = encryptionUtils.encryptAES(jsonData, aesKey);
        
        return EncryptedResponse.of(encryptedData);
    }
    
    /**
     * 移除会话密钥
     */
    public void removeSessionKey(String sessionId) {
        sessionKeyMap.remove(sessionId);
    }
}
