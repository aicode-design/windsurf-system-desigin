package com.windsurf.system.utils;

import com.windsurf.system.domain.excel.*;
import com.windsurf.system.domain.entity.*;
import com.windsurf.system.service.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.Date;

@Component
@RequiredArgsConstructor
public class ExcelConvertUtils {
    
    private final SysDepartmentService departmentService;
    private final SysRoleService roleService;
    private final SysPermissionService permissionService;
    
    /**
     * 将用户Excel数据转换为实体
     */
    public SysUser convertToUser(UserExcel excel) {
        if (excel == null) {
            return null;
        }
        
        SysUser user = new SysUser();
        user.setUsername(excel.getUsername());
        user.setNickname(excel.getNickname());
        user.setPhone(excel.getPhone());
        user.setEmail(excel.getEmail());
        user.setGender(convertGender(excel.getGender()));
        user.setStatus(convertStatus(excel.getStatus()));
        user.setSort(excel.getSort());
        user.setCreateTime(excel.getCreateTime());
        
        // 处理部门
        if (StringUtils.isNotBlank(excel.getDeptName())) {
            SysDepartment department = departmentService.getByName(excel.getDeptName());
            if (department != null) {
                user.setDepartmentId(department.getId());
                user.setDepartmentName(department.getName());
            }
        }
        
        // 处理角色
        if (StringUtils.isNotBlank(excel.getRoleNames())) {
            List<String> roleNames = Arrays.asList(excel.getRoleNames().split(","));
            List<SysRole> roles = roleService.getByNames(roleNames);
            if (roles != null && !roles.isEmpty()) {
                user.setRoleIds(roles.stream().map(SysRole::getId).collect(Collectors.toList()));
                user.setRoleNames(roles.stream().map(SysRole::getName).collect(Collectors.toList()));
            }
        }
        
        return user;
    }
    
    /**
     * 将角色Excel数据转换为实体
     */
    public SysRole convertToRole(RoleExcel excel) {
        if (excel == null) {
            return null;
        }
        
        SysRole role = new SysRole();
        role.setName(excel.getName());
        role.setCode(excel.getCode());
        role.setDescription(excel.getDescription());
        role.setStatus(convertStatus(excel.getStatus()));
        role.setSort(excel.getSort());
        role.setCreateTime(excel.getCreateTime());
        
        // 处理权限
        if (StringUtils.isNotBlank(excel.getPermissionNames())) {
            List<String> permissionNames = Arrays.asList(excel.getPermissionNames().split(","));
            List<SysPermission> permissions = permissionService.getByNames(permissionNames);
            if (permissions != null && !permissions.isEmpty()) {
                role.setPermissionIds(permissions.stream().map(SysPermission::getId).collect(Collectors.toList()));
                role.setPermissionNames(permissions.stream().map(SysPermission::getName).collect(Collectors.toList()));
            }
        }
        
        return role;
    }
    
    /**
     * 将权限Excel数据转换为实体
     */
    public SysPermission convertToPermission(PermissionExcel excel) {
        if (excel == null) {
            return null;
        }
        
        SysPermission permission = new SysPermission();
        permission.setName(excel.getName());
        permission.setCode(excel.getCode());
        permission.setType(convertPermissionType(excel.getType()));
        permission.setPath(excel.getPath());
        permission.setComponent(excel.getComponent());
        permission.setPermission(excel.getPermission());
        permission.setIcon(excel.getIcon());
        permission.setStatus(convertStatus(excel.getStatus()));
        permission.setSort(excel.getSort());
        permission.setCreateTime(excel.getCreateTime());
        
        // 处理父级权限
        if (StringUtils.isNotBlank(excel.getParentName())) {
            SysPermission parent = permissionService.getByName(excel.getParentName());
            if (parent != null) {
                permission.setParentId(parent.getId());
                permission.setParentName(parent.getName());
            }
        }
        
        return permission;
    }
    
    /**
     * 将用户实体转换为Excel数据
     */
    public UserExcel convertFromUser(SysUser user) {
        if (user == null) {
            return null;
        }
        
        UserExcel excel = new UserExcel();
        excel.setUsername(user.getUsername());
        excel.setNickname(user.getNickname());
        excel.setPhone(user.getPhone());
        excel.setEmail(user.getEmail());
        excel.setGender(convertGenderToString(user.getGender()));
        excel.setStatus(convertStatusToString(user.getStatus()));
        excel.setSort(user.getSort());
        excel.setCreateTime(user.getCreateTime());
        
        // 转换部门名称
        excel.setDeptName(user.getDepartmentName());
        
        // 转换角色名称列表
        if (user.getRoleNames() != null && !user.getRoleNames().isEmpty()) {
            excel.setRoleNames(String.join(",", user.getRoleNames()));
        }
        
        return excel;
    }
    
    /**
     * 将角色实体转换为Excel数据
     */
    public RoleExcel convertFromRole(SysRole role) {
        if (role == null) {
            return null;
        }
        
        RoleExcel excel = new RoleExcel();
        excel.setName(role.getName());
        excel.setCode(role.getCode());
        excel.setDescription(role.getDescription());
        excel.setStatus(convertStatusToString(role.getStatus()));
        excel.setSort(role.getSort());
        excel.setCreateTime(role.getCreateTime());
        
        // 转换权限名称列表
        if (role.getPermissionNames() != null && !role.getPermissionNames().isEmpty()) {
            excel.setPermissionNames(String.join(",", role.getPermissionNames()));
        }
        
        return excel;
    }
    
    /**
     * 将权限实体转换为Excel数据
     */
    public PermissionExcel convertFromPermission(SysPermission permission) {
        if (permission == null) {
            return null;
        }
        
        PermissionExcel excel = new PermissionExcel();
        excel.setName(permission.getName());
        excel.setCode(permission.getCode());
        excel.setType(convertPermissionTypeToString(permission.getType()));
        excel.setPath(permission.getPath());
        excel.setComponent(permission.getComponent());
        excel.setPermission(permission.getPermission());
        excel.setIcon(permission.getIcon());
        excel.setStatus(convertStatusToString(permission.getStatus()));
        excel.setSort(permission.getSort());
        excel.setCreateTime(permission.getCreateTime());
        
        // 转换父级权限名称
        excel.setParentName(permission.getParentName());
        
        return excel;
    }
    
    // 状态转换
    private Integer convertStatus(String status) {
        if (StringUtils.isBlank(status)) {
            return 1; // 默认启用
        }
        return "正常".equals(status) || "启用".equals(status) ? 1 : 0;
    }
    
    private String convertStatusToString(Integer status) {
        return status != null && status == 1 ? "正常" : "停用";
    }
    
    // 性别转换
    private Integer convertGender(String gender) {
        if (StringUtils.isBlank(gender)) {
            return 0; // 默认未知
        }
        switch (gender) {
            case "男":
                return 1;
            case "女":
                return 2;
            default:
                return 0;
        }
    }
    
    private String convertGenderToString(Integer gender) {
        if (gender == null) {
            return "未知";
        }
        switch (gender) {
            case 1:
                return "男";
            case 2:
                return "女";
            default:
                return "未知";
        }
    }
    
    // 权限类型转换
    private Integer convertPermissionType(String type) {
        if (StringUtils.isBlank(type)) {
            return 0;
        }
        switch (type) {
            case "目录":
                return 1;
            case "菜单":
                return 2;
            case "按钮":
                return 3;
            default:
                return 0;
        }
    }
    
    private String convertPermissionTypeToString(Integer type) {
        if (type == null) {
            return "未知";
        }
        switch (type) {
            case 1:
                return "目录";
            case 2:
                return "菜单";
            case 3:
                return "按钮";
            default:
                return "未知";
        }
    }
}
