package com.windsurf.system.controller;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.windsurf.system.domain.entity.SysUser;
import com.windsurf.system.exception.Result;
import com.windsurf.system.service.SysUserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@Tag(name = "用户管理")
@RestController
@RequestMapping("/system/user")
@RequiredArgsConstructor
public class SysUserController {
    private final SysUserService userService;

    @Operation(summary = "获取用户列表")
    @GetMapping("/page")
    @PreAuthorize("hasAuthority('system:user:list')")
    public Result<Page<SysUser>> getUserPage(
            @Parameter(description = "页码") @RequestParam(defaultValue = "1") long current,
            @Parameter(description = "每页数量") @RequestParam(defaultValue = "10") long size,
            @Parameter(description = "关键词") @RequestParam(required = false) String keyword) {
        return Result.ok(userService.getUserPage(new Page<>(current, size), keyword));
    }

    @Operation(summary = "获取用户信息")
    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('system:user:query')")
    public Result<SysUser> getUser(@Parameter(description = "用户ID") @PathVariable Long id) {
        return Result.ok(userService.getById(id));
    }

    @Operation(summary = "创建用户")
    @PostMapping
    @PreAuthorize("hasAuthority('system:user:add')")
    public Result<?> createUser(@Valid @RequestBody SysUser user) {
        userService.createUser(user);
        return Result.ok();
    }

    @Operation(summary = "更新用户")
    @PutMapping
    @PreAuthorize("hasAuthority('system:user:edit')")
    public Result<?> updateUser(@Valid @RequestBody SysUser user) {
        userService.updateUser(user);
        return Result.ok();
    }

    @Operation(summary = "删除用户")
    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('system:user:delete')")
    public Result<?> deleteUser(@Parameter(description = "用户ID") @PathVariable Long id) {
        userService.deleteUser(id);
        return Result.ok();
    }

    @Operation(summary = "重置密码")
    @PutMapping("/{id}/password")
    @PreAuthorize("hasAuthority('system:user:edit')")
    public Result<?> resetPassword(
            @Parameter(description = "用户ID") @PathVariable Long id,
            @Parameter(description = "新密码") @RequestParam String password) {
        userService.resetPassword(id, password);
        return Result.ok();
    }

    @Operation(summary = "更新状态")
    @PutMapping("/{id}/status")
    @PreAuthorize("hasAuthority('system:user:edit')")
    public Result<?> updateStatus(
            @Parameter(description = "用户ID") @PathVariable Long id,
            @Parameter(description = "状态") @RequestParam String status) {
        userService.updateStatus(id, status);
        return Result.ok();
    }
}
