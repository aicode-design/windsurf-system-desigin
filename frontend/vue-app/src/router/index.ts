import { createRouter, createWebHistory } from 'vue-router'
import type { RouteRecordRaw } from 'vue-router'
import { useUserStore } from '@/stores/user'
import BasicLayout from '@/layout/BasicLayout.vue'

const routes: RouteRecordRaw[] = [
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/login/index.vue'),
    meta: { title: '登录', requiresAuth: false }
  },
  {
    path: '/',
    component: BasicLayout,
    redirect: '/dashboard',
    children: [
      {
        path: 'dashboard',
        name: 'Dashboard',
        component: () => import('@/views/dashboard/index.vue'),
        meta: { title: '仪表盘', requiresAuth: true }
      },
      {
        path: 'system',
        name: 'System',
        redirect: '/system/user',
        meta: { title: '系统管理', requiresAuth: true },
        children: [
          {
            path: 'user',
            name: 'User',
            component: () => import('@/views/system/user/index.vue'),
            meta: { title: '用户管理', requiresAuth: true, permission: 'system:user:list' }
          },
          {
            path: 'role',
            name: 'Role',
            component: () => import('@/views/system/role/index.vue'),
            meta: { title: '角色管理', requiresAuth: true, permission: 'system:role:list' }
          },
          {
            path: 'menu',
            name: 'Menu',
            component: () => import('@/views/system/menu/index.vue'),
            meta: { title: '菜单管理', requiresAuth: true, permission: 'system:menu:list' }
          },
          {
            path: 'dept',
            name: 'Department',
            component: () => import('@/views/system/dept/index.vue'),
            meta: { title: '部门管理', requiresAuth: true, permission: 'system:dept:list' }
          },
          {
            path: 'api',
            name: 'API',
            component: () => import('@/views/system/api/index.vue'),
            meta: { title: 'API管理', requiresAuth: true, permission: 'system:api:list' }
          }
        ]
      },
      {
        path: 'profile',
        name: 'Profile',
        component: () => import('@/views/profile/index.vue'),
        meta: { title: '个人信息', requiresAuth: true }
      }
    ]
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import('@/views/error/404.vue'),
    meta: { title: '404', requiresAuth: false }
  }
]

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes
})

// 路由守卫
router.beforeEach(async (to, from, next) => {
  // 设置页面标题
  document.title = `${to.meta.title} - Admin System`

  const userStore = useUserStore()
  const token = userStore.token || localStorage.getItem('token')

  if (to.meta.requiresAuth) {
    if (!token) {
      next({ name: 'Login', query: { redirect: to.fullPath } })
      return
    }

    // 如果没有用户信息，获取用户信息
    if (!userStore.userInfo) {
      try {
        await userStore.getUserInfo()
      } catch (error) {
        next({ name: 'Login', query: { redirect: to.fullPath } })
        return
      }
    }

    // 检查权限
    if (to.meta.permission) {
      const hasPermission = userStore.permissions.includes(to.meta.permission as string)
      if (!hasPermission) {
        next({ name: '403' })
        return
      }
    }

    next()
  } else {
    if (token && to.path === '/login') {
      next({ path: '/' })
      return
    }
    next()
  }
})

export default router
