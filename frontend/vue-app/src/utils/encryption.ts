import CryptoJS from 'crypto-js'
import JSEncrypt from 'jsencrypt'

/**
 * 加密工具类
 */
export class EncryptionUtils {
  private static instance: EncryptionUtils
  private serverPublicKey: string | null = null
  private aesKey: string | null = null
  private jsEncrypt: JSEncrypt

  private constructor() {
    this.jsEncrypt = new JSEncrypt()
  }

  public static getInstance(): EncryptionUtils {
    if (!EncryptionUtils.instance) {
      EncryptionUtils.instance = new EncryptionUtils()
    }
    return EncryptionUtils.instance
  }

  /**
   * 初始化加密工具
   * @param publicKey RSA公钥
   */
  public initialize(publicKey: string) {
    this.serverPublicKey = publicKey
    this.jsEncrypt.setPublicKey(publicKey)
    // 生成随机的AES密钥
    this.aesKey = CryptoJS.lib.WordArray.random(32).toString()
  }

  /**
   * 重置加密工具
   */
  public reset() {
    this.serverPublicKey = null
    this.aesKey = null
  }

  /**
   * 加密请求数据
   * @param data 原始数据
   */
  public encryptRequest(data: any): { encryptedKey: string; encryptedData: string } {
    if (!this.serverPublicKey || !this.aesKey) {
      throw new Error('Encryption not initialized')
    }

    // 使用AES加密数据
    const encryptedData = CryptoJS.AES.encrypt(
      JSON.stringify(data),
      this.aesKey
    ).toString()

    // 使用RSA加密AES密钥
    const encryptedKey = this.jsEncrypt.encrypt(this.aesKey)
    if (!encryptedKey) {
      throw new Error('Failed to encrypt AES key')
    }

    return {
      encryptedKey,
      encryptedData
    }
  }

  /**
   * 解密响应数据
   * @param encryptedData 加密的数据
   */
  public decryptResponse(encryptedData: string): any {
    if (!this.aesKey) {
      throw new Error('Encryption not initialized')
    }

    // 使用AES密钥解密数据
    const bytes = CryptoJS.AES.decrypt(encryptedData, this.aesKey)
    const decryptedData = bytes.toString(CryptoJS.enc.Utf8)
    
    try {
      return JSON.parse(decryptedData)
    } catch (e) {
      throw new Error('Failed to decrypt response data')
    }
  }
}

// 导出单例实例
export const encryptionUtils = EncryptionUtils.getInstance()
