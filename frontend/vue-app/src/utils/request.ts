import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { message } from 'ant-design-vue'
import { encryptionUtils } from './encryption'

// 需要加密的请求路径
const ENCRYPTED_PATHS = [
  '/auth/login',
  '/auth/info'
]

const service = axios.create({
  baseURL: '/api',
  timeout: 10000
})

// 初始化加密工具
let encryptionInitialized = false
const initializeEncryption = async () => {
  if (!encryptionInitialized) {
    try {
      const response = await axios.get('/api/auth/public-key')
      if (response.data.code === 0) {
        encryptionUtils.initialize(response.data.data)
        encryptionInitialized = true
      }
    } catch (error) {
      console.error('Failed to initialize encryption:', error)
      throw error
    }
  }
}

// 请求拦截器
service.interceptors.request.use(
  async (config) => {
    const token = localStorage.getItem('token')
    if (token) {
      config.headers['Authorization'] = `Bearer ${token}`
    }

    // 检查是否需要加密
    if (ENCRYPTED_PATHS.some(path => config.url?.includes(path))) {
      await initializeEncryption()
      const originalData = config.data
      const encryptedRequest = encryptionUtils.encryptRequest(originalData)
      config.data = encryptedRequest
    }

    return config
  },
  (error) => {
    return Promise.reject(error)
  }
)

// 响应拦截器
service.interceptors.response.use(
  (response: AxiosResponse) => {
    const res = response.data
    if (res.code !== 0) {
      message.error(res.message || 'Error')
      if (res.code === 401) {
        // 清理加密状态
        encryptionUtils.reset()
        encryptionInitialized = false
        // 处理未授权
        localStorage.removeItem('token')
        window.location.href = '/login'
      }
      return Promise.reject(new Error(res.message || 'Error'))
    }

    // 检查是否需要解密
    if (ENCRYPTED_PATHS.some(path => response.config.url?.includes(path))) {
      try {
        const decryptedData = encryptionUtils.decryptResponse(res.data.encryptedData)
        return {
          ...res,
          data: decryptedData
        }
      } catch (error) {
        console.error('Failed to decrypt response:', error)
        return Promise.reject(error)
      }
    }

    return res
  },
  (error) => {
    // 清理加密状态
    if (error.response?.status === 401) {
      encryptionUtils.reset()
      encryptionInitialized = false
    }
    message.error(error.message || 'Request Error')
    return Promise.reject(error)
  }
)

export default service
