import { defineStore } from 'pinia'
import { ref } from 'vue'
import type { UserInfo } from '@/types/user'
import request from '@/utils/request'

export const useUserStore = defineStore('user', () => {
  const userInfo = ref<UserInfo | null>(null)
  const token = ref<string | null>(null)
  const permissions = ref<string[]>([])

  const login = async (username: string, password: string) => {
    try {
      // 登录请求会自动加密
      const res = await request.post('/auth/login', { username, password })
      token.value = res.data.token
      localStorage.setItem('token', res.data.token)
      await getUserInfo()
      return res
    } catch (error) {
      resetState()
      return Promise.reject(error)
    }
  }

  const getUserInfo = async () => {
    try {
      // 获取用户信息请求会自动加密
      const res = await request.get('/auth/info')
      userInfo.value = res.data
      permissions.value = res.data.permissions || []
      return res
    } catch (error) {
      resetState()
      return Promise.reject(error)
    }
  }

  const logout = async () => {
    try {
      await request.post('/auth/logout')
      resetState()
    } catch (error) {
      resetState()
      return Promise.reject(error)
    }
  }

  const resetState = () => {
    userInfo.value = null
    token.value = null
    permissions.value = []
    localStorage.removeItem('token')
  }

  const hasPermission = (permission: string): boolean => {
    return permissions.value.includes(permission)
  }

  const hasRole = (role: string): boolean => {
    return userInfo.value?.roleNames?.includes(role) || false
  }

  return {
    userInfo,
    token,
    permissions,
    login,
    getUserInfo,
    logout,
    resetState,
    hasPermission,
    hasRole
  }
})
