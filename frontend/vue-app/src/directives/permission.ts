import type { Directive, DirectiveBinding } from 'vue'
import { useUserStore } from '@/stores/user'

/**
 * 权限指令
 * 使用方式：
 * 1. 单个权限: v-permission="'system:user:add'"
 * 2. 多个权限(或): v-permission="['system:user:add', 'system:user:edit']"
 * 3. 多个权限(与): v-permission.all="['system:user:add', 'system:user:edit']"
 * 4. 角色判断: v-permission.role="'admin'"
 * 5. 多个角色(或): v-permission.role="['admin', 'manager']"
 * 6. 多个角色(与): v-permission.role.all="['admin', 'manager']"
 */
export const permission: Directive = {
  mounted(el: HTMLElement, binding: DirectiveBinding) {
    const { value, modifiers } = binding
    const userStore = useUserStore()
    
    // 判断是否为角色权限
    if (modifiers.role) {
      const roles = Array.isArray(value) ? value : [value]
      const hasRole = modifiers.all
        ? roles.every(role => userStore.hasRole(role))
        : roles.some(role => userStore.hasRole(role))
      
      if (!hasRole) {
        el.parentNode?.removeChild(el)
      }
      return
    }
    
    // 权限判断
    if (value) {
      const permissions = Array.isArray(value) ? value : [value]
      const hasPermission = modifiers.all
        ? permissions.every(permission => userStore.hasPermission(permission))
        : permissions.some(permission => userStore.hasPermission(permission))
      
      if (!hasPermission) {
        el.parentNode?.removeChild(el)
      }
    } else {
      throw new Error(`need permission! Like v-permission="'system:user:add'"`)
    }
  }
}

export default permission
