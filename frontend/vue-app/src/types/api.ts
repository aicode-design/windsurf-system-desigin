// API接口定义
export interface API {
  id: string
  name: string
  path: string
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH'
  description?: string
  module: string
  category: string
  status: 'active' | 'inactive'
  needAuth: boolean
  needPermission: boolean
  permissionCode?: string
  createTime?: string
  updateTime?: string
}

// API查询参数
export interface APIQuery {
  name?: string
  path?: string
  method?: string
  module?: string
  category?: string
  status?: string
  startTime?: string
  endTime?: string
}

// API表单数据
export interface APIForm {
  id?: string
  name: string
  path: string
  method: 'GET' | 'POST' | 'PUT' | 'DELETE' | 'PATCH'
  description?: string
  module: string
  category: string
  status: 'active' | 'inactive'
  needAuth: boolean
  needPermission: boolean
  permissionCode?: string
}

// API统计数据
export interface APIStats {
  total: number
  active: number
  inactive: number
  byMethod: {
    GET: number
    POST: number
    PUT: number
    DELETE: number
    PATCH: number
  }
  byModule: Record<string, number>
}
