// 组织架构接口定义
export interface Organization {
  id: string
  name: string
  code: string
  parentId?: string
  type: 'company' | 'department' | 'team'
  leader?: string
  phone?: string
  email?: string
  address?: string
  orderNum: number
  status: 'active' | 'inactive'
  createTime?: string
  updateTime?: string
  children?: Organization[]
}

// 组织架构查询参数
export interface OrganizationQuery {
  name?: string
  code?: string
  type?: string
  status?: string
  startTime?: string
  endTime?: string
}

// 组织架构表单数据
export interface OrganizationForm {
  id?: string
  name: string
  code: string
  parentId?: string
  type: 'company' | 'department' | 'team'
  leader?: string
  phone?: string
  email?: string
  address?: string
  orderNum: number
  status: 'active' | 'inactive'
}

// 组织架构树节点
export interface OrgTreeNode extends Organization {
  title: string
  value: string
  key: string
  children?: OrgTreeNode[]
}
