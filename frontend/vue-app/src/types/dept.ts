// 部门接口定义
export interface Department {
  id: string
  name: string
  parentId?: string
  orderNum: number
  leader?: string
  phone?: string
  email?: string
  status: 'active' | 'inactive'
  createTime?: string
  updateTime?: string
  children?: Department[]
}

// 部门查询参数
export interface DepartmentQuery {
  name?: string
  status?: string
  startTime?: string
  endTime?: string
}

// 部门表单数据
export interface DepartmentForm {
  id?: string
  name: string
  parentId?: string
  orderNum: number
  leader?: string
  phone?: string
  email?: string
  status: 'active' | 'inactive'
}
