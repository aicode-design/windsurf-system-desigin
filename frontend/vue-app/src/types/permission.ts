// 权限接口定义
export interface Permission {
  id: string
  name: string
  code: string
  description?: string
  type: 'menu' | 'button' | 'api'
  status: 'active' | 'inactive'
  createTime?: string
  updateTime?: string
}

// 权限查询参数
export interface PermissionQuery {
  name?: string
  code?: string
  type?: string
  status?: string
  startTime?: string
  endTime?: string
}

// 权限表单数据
export interface PermissionForm {
  id?: string
  name: string
  code: string
  description?: string
  type: 'menu' | 'button' | 'api'
  status: 'active' | 'inactive'
}

// 权限分配
export interface PermissionAssignment {
  roleId: string
  permissionIds: string[]
}
