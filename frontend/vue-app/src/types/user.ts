export interface UserInfo {
  id: number
  username: string
  nickname: string
  email: string
  phone: string
  avatar: string
  gender: number
  status: number
  sort: number
  departmentId: number
  organizationId: number
  departmentName: string
  roleNames: string[]
  permissions: string[]
}

export interface LoginRequest {
  username: string
  password: string
}

export interface LoginResponse {
  token: string
}

export interface EncryptedRequest {
  encryptedKey: string
  encryptedData: string
}

export interface EncryptedResponse {
  encryptedData: string
}

export enum Gender {
  UNKNOWN = 0,
  MALE = 1,
  FEMALE = 2
}

export enum Status {
  DISABLED = 0,
  ENABLED = 1
}
