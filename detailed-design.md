# RBAC+ABAC混合权限管理系统详细设计文档

## 目录

1. [系统详细设计](#1-系统详细设计)
2. [时序图](#2-时序图)
3. [部署架构](#3-部署架构)
4. [安全设计](#4-安全设计)
5. [性能优化](#5-性能优化)
6. [测试策略](#6-测试策略)
7. [错误处理](#7-错误处理)
8. [系统配置](#8-系统配置)
9. [部署指南](#9-部署指南)
10. [维护指南](#10-维护指南)
11. [TypeScript实现](#11-typescript实现)
12. [前端实现](#12-前端实现)
13. [API文档](#13-api文档)
14. [数据迁移](#14-数据迁移)
15. [监控告警](#15-监控告警)
16. [多语言实现](#16-多语言实现)
17. [组织架构管理](#17-组织架构管理)
18. [部门管理](#18-部门管理)
19. [菜单管理](#19-菜单管理)
20. [API管理](#20-api管理)

## 1. 系统详细设计

### 1.1 核心类设计

#### User 类
```typescript
interface User {
    id: string;
    username: string;
    email: string;
    password: string;
    roles: Role[];
    attributes: UserAttributes;
    status: UserStatus;
    createdAt: Date;
    updatedAt: Date;
}

interface UserAttributes {
    department: string;
    position: string;
    level: number;
    region: string;
}

enum UserStatus {
    ACTIVE = 'active',
    INACTIVE = 'inactive',
    LOCKED = 'locked'
}
```

#### Role 类
```typescript
interface Role {
    id: string;
    name: string;
    description: string;
    permissions: Permission[];
    attributes: RoleAttributes;
    createdAt: Date;
    updatedAt: Date;
}

interface RoleAttributes {
    level: number;
    scope: string[];
    department: string;
}
```

#### Permission 类
```typescript
interface Permission {
    id: string;
    name: string;
    resourceType: string;
    actions: string[];
    conditions: Condition[];
    attributes: PermissionAttributes;
}

interface Condition {
    type: ConditionType;
    field: string;
    operator: ConditionOperator;
    value: any;
}

interface PermissionAttributes {
    priority: number;
    scope: string[];
    expiration?: Date;
}
```

### 1.2 数据库表详细设计

#### users 表
| 字段名 | 类型 | 说明 | 索引 |
|--------|------|------|------|
| id | varchar(36) | 主键 | PRIMARY |
| username | varchar(50) | 用户名 | UNIQUE |
| email | varchar(100) | 邮箱 | UNIQUE |
| password | varchar(100) | 加密密码 | - |
| status | varchar(20) | 用户状态 | INDEX |
| created_at | timestamp | 创建时间 | - |
| updated_at | timestamp | 更新时间 | - |

#### roles 表
| 字段名 | 类型 | 说明 | 索引 |
|--------|------|------|------|
| id | varchar(36) | 主键 | PRIMARY |
| name | varchar(50) | 角色名 | UNIQUE |
| description | text | 描述 | - |
| created_at | timestamp | 创建时间 | - |
| updated_at | timestamp | 更新时间 | - |

#### permissions 表
| 字段名 | 类型 | 说明 | 索引 |
|--------|------|------|------|
| id | varchar(36) | 主键 | PRIMARY |
| name | varchar(100) | 权限名 | UNIQUE |
| resource_type | varchar(50) | 资源类型 | INDEX |
| actions | json | 操作列表 | - |
| conditions | json | 条件列表 | - |
| created_at | timestamp | 创建时间 | - |

### 1.3 API详细设计

#### 认证相关API
```typescript
interface AuthAPI {
    login(username: string, password: string): Promise<AuthToken>;
    logout(token: string): Promise<void>;
    refreshToken(refreshToken: string): Promise<AuthToken>;
    validateToken(token: string): Promise<boolean>;
}
```

#### 用户管理API
```typescript
interface UserAPI {
    createUser(user: CreateUserDTO): Promise<User>;
    updateUser(id: string, user: UpdateUserDTO): Promise<User>;
    deleteUser(id: string): Promise<void>;
    getUser(id: string): Promise<User>;
    listUsers(query: UserQueryDTO): Promise<UserList>;
}
```

#### 权限管理API
```typescript
interface PermissionAPI {
    checkPermission(userId: string, resource: string, action: string): Promise<boolean>;
    grantPermission(roleId: string, permission: GrantPermissionDTO): Promise<void>;
    revokePermission(roleId: string, permissionId: string): Promise<void>;
    listPermissions(roleId: string): Promise<Permission[]>;
}
```

## 2. 时序图

### 2.1 权限验证流程
```mermaid
sequenceDiagram
    participant Client
    participant AuthMiddleware
    participant PermissionService
    participant Cache
    participant Database

    Client->>AuthMiddleware: 请求资源
    AuthMiddleware->>PermissionService: 验证权限
    PermissionService->>Cache: 查询缓存
    alt 缓存命中
        Cache-->>PermissionService: 返回权限信息
    else 缓存未命中
        PermissionService->>Database: 查询数据库
        Database-->>PermissionService: 返回权限信息
        PermissionService->>Cache: 更新缓存
    end
    PermissionService-->>AuthMiddleware: 权限验证结果
    AuthMiddleware-->>Client: 响应结果
```

## 3. 部署架构

### 3.1 容器化部署

使用Docker和Kubernetes进行容器化部署：

```yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rbac-service
spec:
  replicas: 3
  selector:
    matchLabels:
      app: rbac-service
  template:
    metadata:
      labels:
        app: rbac-service
    spec:
      containers:
      - name: rbac-service
        image: rbac-service:latest
        ports:
        - containerPort: 8080
        env:
        - name: DB_HOST
          valueFrom:
            configMapKeyRef:
              name: rbac-config
              key: db_host
```

### 3.2 高可用设计

- 使用Kubernetes进行容器编排
- 实现服务多副本部署
- 配置健康检查和自动恢复
- 使用Redis集群作为缓存层
- 数据库主从复制

## 4. 安全设计

### 4.1 密码安全
- 使用bcrypt进行密码加密
- 实现密码强度校验
- 密码定期更新机制
- 密码重试次数限制

### 4.2 JWT配置
```typescript
interface JWTConfig {
    secret: string;
    expiresIn: string;
    algorithm: 'HS256' | 'RS256';
    issuer: string;
}
```

## 5. 性能优化

### 5.1 数据库索引优化
- 用户表索引优化
- 角色权限关联表索引
- 查询语句优化

### 5.2 缓存策略
```typescript
interface CacheStrategy {
    type: 'Redis' | 'Memory';
    ttl: number;
    maxSize: number;
    evictionPolicy: 'LRU' | 'LFU';
}
```

## 6. 测试策略

### 6.1 单元测试
```typescript
describe('PermissionService', () => {
    it('should validate user permission correctly', async () => {
        const result = await permissionService.validatePermission(userId, 'resource', 'action');
        expect(result).toBeTruthy();
    });
});
```

## 7. 错误处理

### 7.1 全局异常处理
```typescript
@Catch()
export class GlobalExceptionFilter implements ExceptionFilter {
    catch(exception: any, host: ArgumentsHost) {
        const ctx = host.switchToHttp();
        const response = ctx.getResponse();
        const status = exception.getStatus?.() || 500;

        response.status(status).json({
            statusCode: status,
            timestamp: new Date().toISOString(),
            message: exception.message || 'Internal server error',
        });
    }
}
```

## 8. 系统配置

### 8.1 应用配置
```typescript
interface AppConfig {
    port: number;
    env: 'development' | 'production' | 'test';
    logLevel: 'debug' | 'info' | 'warn' | 'error';
    corsOptions: CorsOptions;
}
```

## 9. 部署指南

### 9.1 环境要求
- Node.js >= 14.x
- Redis >= 6.x
- PostgreSQL >= 12.x
- Docker >= 20.x
- Kubernetes >= 1.20.x

## 10. 维护指南

### 10.1 日常维护任务
- 日志监控和清理
- 数据库备份
- 性能监控
- 安全漏洞扫描

## 11. TypeScript实现

### 11.1 核心服务实现
```typescript
@Injectable()
export class PermissionService {
    constructor(
        private readonly userRepository: UserRepository,
        private readonly roleRepository: RoleRepository,
        private readonly cacheService: CacheService,
    ) {}

    async validatePermission(userId: string, resource: string, action: string): Promise<boolean> {
        const user = await this.userRepository.findById(userId);
        if (!user) return false;

        const permissions = await this.getUserPermissions(user);
        return this.evaluatePermissions(permissions, resource, action);
    }
}
```

## 12. 前端实现

### 12.1 组件设计
```typescript
interface PermissionState {
    permissions: Permission[];
    loading: boolean;
    error: Error | null;
}

const usePermissions = () => {
    const [state, setState] = useState<PermissionState>({
        permissions: [],
        loading: false,
        error: null,
    });

    // 权限相关逻辑实现
};
```

## 13. API文档

### 13.1 认证接口
```typescript
/**
 * @api {post} /auth/login 用户登录
 * @apiName Login
 * @apiGroup Auth
 * @apiParam {String} username 用户名
 * @apiParam {String} password 密码
 * @apiSuccess {String} token JWT token
 */
```

## 14. 数据迁移

### 14.1 迁移策略
- 增量迁移
- 数据验证
- 回滚机制
- 性能优化

## 15. 监控告警

### 15.1 监控指标
- 系统性能指标
- 业务指标
- 安全指标
- 用户行为指标

## 16. 多语言实现

### 16.1 Python实现
```python
class PermissionService:
    def __init__(self, user_repository, role_repository, cache_service):
        self.user_repository = user_repository
        self.role_repository = role_repository
        self.cache_service = cache_service

    async def validate_permission(self, user_id: str, resource: str, action: str) -> bool:
        user = await self.user_repository.find_by_id(user_id)
        if not user:
            return False
        
        permissions = await self.get_user_permissions(user)
        return self.evaluate_permissions(permissions, resource, action)
```

### 16.2 Golang实现
```go
type PermissionService struct {
    userRepository  UserRepository
    roleRepository  RoleRepository
    cacheService    CacheService
}

func (s *PermissionService) ValidatePermission(userId string, resource string, action string) bool {
    user, err := s.userRepository.FindById(userId)
    if err != nil {
        return false
    }

    permissions := s.GetUserPermissions(user)
    return s.EvaluatePermissions(permissions, resource, action)
}
```

## 17. 组织架构管理

### 17.1 组织架构模型
```typescript
interface Organization {
    id: string;
    name: string;
    code: string;
    parentId: string | null;
    path: string;
    level: number;
    sort: number;
    status: OrganizationStatus;
    description: string;
    createdAt: Date;
    updatedAt: Date;
}

enum OrganizationStatus {
    ACTIVE = 'active',
    INACTIVE = 'inactive'
}

interface OrganizationTree {
    id: string;
    name: string;
    code: string;
    children: OrganizationTree[];
    level: number;
    path: string;
}
```

### 17.2 数据库设计

#### organizations 表
| 字段名 | 类型 | 说明 | 索引 |
|--------|------|------|------|
| id | varchar(36) | 主键 | PRIMARY |
| name | varchar(100) | 组织名称 | INDEX |
| code | varchar(50) | 组织编码 | UNIQUE |
| parent_id | varchar(36) | 父组织ID | INDEX |
| path | varchar(255) | 组织路径 | INDEX |
| level | int | 组织层级 | INDEX |
| sort | int | 排序号 | - |
| status | varchar(20) | 状态 | INDEX |
| description | text | 描述 | - |
| created_at | timestamp | 创建时间 | - |
| updated_at | timestamp | 更新时间 | - |

### 17.3 API设计
```typescript
interface OrganizationAPI {
    createOrganization(org: CreateOrganizationDTO): Promise<Organization>;
    updateOrganization(id: string, org: UpdateOrganizationDTO): Promise<Organization>;
    deleteOrganization(id: string): Promise<void>;
    getOrganization(id: string): Promise<Organization>;
    getOrganizationTree(): Promise<OrganizationTree>;
    moveOrganization(id: string, parentId: string): Promise<void>;
}
```

## 18. 部门管理

### 18.1 部门模型
```typescript
interface Department {
    id: string;
    name: string;
    code: string;
    organizationId: string;
    parentId: string | null;
    path: string;
    level: number;
    sort: number;
    manager: string;
    status: DepartmentStatus;
    description: string;
    createdAt: Date;
    updatedAt: Date;
}

enum DepartmentStatus {
    ACTIVE = 'active',
    INACTIVE = 'inactive'
}

interface DepartmentTree {
    id: string;
    name: string;
    code: string;
    children: DepartmentTree[];
    level: number;
    path: string;
    manager: string;
}
```

### 18.2 数据库设计

#### departments 表
| 字段名 | 类型 | 说明 | 索引 |
|--------|------|------|------|
| id | varchar(36) | 主键 | PRIMARY |
| name | varchar(100) | 部门名称 | INDEX |
| code | varchar(50) | 部门编码 | UNIQUE |
| organization_id | varchar(36) | 所属组织ID | INDEX |
| parent_id | varchar(36) | 父部门ID | INDEX |
| path | varchar(255) | 部门路径 | INDEX |
| level | int | 部门层级 | INDEX |
| sort | int | 排序号 | - |
| manager | varchar(36) | 部门主管ID | INDEX |
| status | varchar(20) | 状态 | INDEX |
| description | text | 描述 | - |
| created_at | timestamp | 创建时间 | - |
| updated_at | timestamp | 更新时间 | - |

### 18.3 API设计
```typescript
interface DepartmentAPI {
    createDepartment(dept: CreateDepartmentDTO): Promise<Department>;
    updateDepartment(id: string, dept: UpdateDepartmentDTO): Promise<Department>;
    deleteDepartment(id: string): Promise<void>;
    getDepartment(id: string): Promise<Department>;
    getDepartmentTree(organizationId: string): Promise<DepartmentTree>;
    moveDepartment(id: string, parentId: string): Promise<void>;
    assignManager(id: string, managerId: string): Promise<void>;
}
```

## 19. 菜单管理

### 19.1 菜单模型
```typescript
interface Menu {
    id: string;
    name: string;
    code: string;
    parentId: string | null;
    path: string;
    component: string;
    icon: string;
    sort: number;
    type: MenuType;
    permission: string;
    status: MenuStatus;
    hidden: boolean;
    createdAt: Date;
    updatedAt: Date;
}

enum MenuType {
    DIRECTORY = 'directory',
    MENU = 'menu',
    BUTTON = 'button'
}

enum MenuStatus {
    ACTIVE = 'active',
    INACTIVE = 'inactive'
}

interface MenuTree {
    id: string;
    name: string;
    children: MenuTree[];
    type: MenuType;
    path: string;
    component: string;
    icon: string;
}
```

### 19.2 数据库设计

#### menus 表
| 字段名 | 类型 | 说明 | 索引 |
|--------|------|------|------|
| id | varchar(36) | 主键 | PRIMARY |
| name | varchar(100) | 菜单名称 | INDEX |
| code | varchar(50) | 菜单编码 | UNIQUE |
| parent_id | varchar(36) | 父菜单ID | INDEX |
| path | varchar(255) | 路由路径 | INDEX |
| component | varchar(255) | 组件路径 | - |
| icon | varchar(50) | 图标 | - |
| sort | int | 排序号 | - |
| type | varchar(20) | 菜单类型 | INDEX |
| permission | varchar(100) | 权限标识 | INDEX |
| status | varchar(20) | 状态 | INDEX |
| hidden | boolean | 是否隐藏 | - |
| created_at | timestamp | 创建时间 | - |
| updated_at | timestamp | 更新时间 | - |

### 19.3 API设计
```typescript
interface MenuAPI {
    createMenu(menu: CreateMenuDTO): Promise<Menu>;
    updateMenu(id: string, menu: UpdateMenuDTO): Promise<Menu>;
    deleteMenu(id: string): Promise<void>;
    getMenu(id: string): Promise<Menu>;
    getMenuTree(): Promise<MenuTree>;
    getUserMenus(userId: string): Promise<MenuTree>;
    moveMenu(id: string, parentId: string): Promise<void>;
}
```

## 20. API管理

### 20.1 API模型
```typescript
interface API {
    id: string;
    name: string;
    method: HttpMethod;
    path: string;
    group: string;
    description: string;
    status: APIStatus;
    auth: boolean;
    rateLimit: number;
    timeout: number;
    createdAt: Date;
    updatedAt: Date;
}

enum HttpMethod {
    GET = 'GET',
    POST = 'POST',
    PUT = 'PUT',
    DELETE = 'DELETE',
    PATCH = 'PATCH'
}

enum APIStatus {
    ONLINE = 'online',
    OFFLINE = 'offline',
    DEPRECATED = 'deprecated'
}

interface APIGroup {
    name: string;
    apis: API[];
    description: string;
}
```

### 20.2 数据库设计

#### apis 表
| 字段名 | 类型 | 说明 | 索引 |
|--------|------|------|------|
| id | varchar(36) | 主键 | PRIMARY |
| name | varchar(100) | API名称 | INDEX |
| method | varchar(10) | HTTP方法 | INDEX |
| path | varchar(255) | API路径 | INDEX |
| group | varchar(50) | API分组 | INDEX |
| description | text | 描述 | - |
| status | varchar(20) | 状态 | INDEX |
| auth | boolean | 是否需要认证 | - |
| rate_limit | int | 速率限制 | - |
| timeout | int | 超时时间(ms) | - |
| created_at | timestamp | 创建时间 | - |
| updated_at | timestamp | 更新时间 | - |

### 20.3 API设计
```typescript
interface APIManagementAPI {
    createAPI(api: CreateAPIDTO): Promise<API>;
    updateAPI(id: string, api: UpdateAPIDTO): Promise<API>;
    deleteAPI(id: string): Promise<void>;
    getAPI(id: string): Promise<API>;
    listAPIs(query: APIQueryDTO): Promise<APIList>;
    getAPIGroups(): Promise<APIGroup[]>;
    toggleAPIStatus(id: string, status: APIStatus): Promise<void>;
}

interface APIQueryDTO {
    group?: string;
    method?: HttpMethod;
    status?: APIStatus;
    auth?: boolean;
    page: number;
    size: number;
}

interface APIList {
    total: number;
    items: API[];
}
```

### 20.4 安全配置
```typescript
interface APISecurityConfig {
    rateLimit: {
        enabled: boolean;
        windowMs: number;
        max: number;
    };
    auth: {
        required: boolean;
        scopes: string[];
    };
    cors: {
        enabled: boolean;
        origins: string[];
        methods: string[];
    };
}
